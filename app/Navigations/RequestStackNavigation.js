import {
    createStackNavigator,
} from 'react-navigation';

import Request from '../Screens/Request/Request';

export default RequestStack = createStackNavigator(
    {
        Request: { screen: Request },
    },
    {
        initialRouteName: 'Request',
        defaultNavigationOptions: {
			gesturesEnabled: false,
            headerStyle: {
                backgroundColor: '#42f44b',
            },
            headerTintColor: '#FFFFFF',
            title: 'Search',
        }
    }
);

RequestStack.navigationOptions = ({navigation}) => {
    let tabBarVisible = false;

    let routeName = navigation.state.routes[navigation.state.index].routeName;

    if(routeName === 'Request'){
        tabBarVisible = true;
    }

    return {
        tabBarVisible
    };
}