import {
    createStackNavigator,
} from 'react-navigation';

import NewsFeed from '../Screens/NewsFeed/NewsFeed';

export default NewsFeedStack = createStackNavigator(
    {
        NewsFeed: { screen: NewsFeed },
    },
    {
        initialRouteName: 'NewsFeed',
        defaultNavigationOptions: {
			gesturesEnabled: false,
            headerStyle: {
                backgroundColor: '#42f44b',
            },
            headerTintColor: '#FFFFFF',
            title: 'Search',
        }
    }
);

NewsFeedStack.navigationOptions = ({navigation}) => {
    let tabBarVisible = false;

    let routeName = navigation.state.routes[navigation.state.index].routeName;

    if(routeName === 'NewsFeed'){
        tabBarVisible = true;
    }

    return {
        tabBarVisible
    };
}