import {
    createStackNavigator,
} from 'react-navigation';

import Profile from '../Screens/Profile/Profile';

export default ProfileStack = createStackNavigator(
    {
        Profile: { screen: Profile },
    },
    {
        initialRouteName: 'Profile',
        defaultNavigationOptions: {
			gesturesEnabled: false,
            headerStyle: {
                backgroundColor: '#42f44b',
            },
            headerTintColor: '#FFFFFF',
            title: 'Search',
        }
    }
);

ProfileStack.navigationOptions = ({navigation}) => {
    let tabBarVisible = false;

    let routeName = navigation.state.routes[navigation.state.index].routeName;

    if(routeName === 'Profile'){
        tabBarVisible = true;
    }

    return {
        tabBarVisible
    };
}