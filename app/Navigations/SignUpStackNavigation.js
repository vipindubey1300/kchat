import { createStackNavigator } from "react-navigation";

import SignUpPhoneNumber from '../Screens/SignUp/SignUpPhoneNumber';
import SignUpVerification from '../Screens/SignUp/SignUpVerification';
import SignUpProfile from '../Screens/SignUp/SignUpProfile';

export default SignUpStackNavigation = createStackNavigator({
		SignUpPhoneNumber: SignUpPhoneNumber,
		SignUpVerification: SignUpVerification,
		SignUpProfile: SignUpProfile,
	},
	{
		initialRouteName :'SignUpPhoneNumber',
		defaultNavigationOptions: {
			gesturesEnabled: false,
		},
	}
);