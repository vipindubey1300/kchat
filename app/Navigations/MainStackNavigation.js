import React from 'react';
import {
	View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import {
  	createBottomTabNavigator,
} from 'react-navigation';

import Colors from '../Constants/Colors';

import ProfileStackNavigation from './ProfileStackNavigation';
import NewsFeedStackNavigation from './NewsFeedStackNavigation';
import PhotoStackNavigation from './PhotoStackNavigation';
import RequestStackNavigation from './RequestStackNavigation';
import ChatStackNavigation from './ChatStackNavigation';

export default MainStackNavigation = createBottomTabNavigator(
	{
		NewsFeed: { screen: NewsFeedStackNavigation,
			navigationOptions:{
				tabBarLabel: "News Feed",
                tabBarIcon: ({ focused, tintColor }) => (
					focused ? <FastImage
						style={{ width: 60, height: 60 }}
						source={require('../Assets/Image/news_selected.png')}
						resizeMode={FastImage.resizeMode.contain}
					/>
					:
					<FastImage
						style={{ width: 60, height: 60 }}
						source={require('../Assets/Image/news_normal.png')}
						resizeMode={FastImage.resizeMode.contain}
					/>
                )
			}
		},
		Request: { screen: RequestStackNavigation,
			navigationOptions:{
				tabBarLabel: "Requests",
                tabBarIcon: ({ focused, tintColor }) => (
					focused ? <FastImage
						style={{ width: 60, height: 60 }}
						source={require('../Assets/Image/requests_selected.png')}
						resizeMode={FastImage.resizeMode.contain}
					/>
					:
					<FastImage
						style={{ width: 60, height: 60 }}
						source={require('../Assets/Image/requests_normal.png')}
						resizeMode={FastImage.resizeMode.contain}
					/>
                )
			}
		},
		Photo: { screen: PhotoStackNavigation,
			navigationOptions:{
				showLabel: false,
				tabBarLabel: () => {},
                tabBarIcon: ({ focused, tintColor }) => (
					focused ? <FastImage
						style={{ width: 80, height: 80 }}
						source={require('../Assets/Image/camera_selected.png')}
						resizeMode={FastImage.resizeMode.contain}
					/>
					:
					<FastImage
						style={{ width: 80, height: 80 }}
						source={require('../Assets/Image/camera_normal.png')}
						resizeMode={FastImage.resizeMode.contain}
					/>
                )
			}
		},
		Chat: { screen: ChatStackNavigation,
			navigationOptions:{
				tabBarLabel: "Chats",
                tabBarIcon: ({ focused, tintColor }) => (
					focused ? <FastImage
						style={{ width: 60, height: 60 }}
						source={require('../Assets/Image/chats_selected.png')}
						resizeMode={FastImage.resizeMode.contain}
					/>
					:
					<FastImage
						style={{ width: 60, height: 60 }}
						source={require('../Assets/Image/chats_normal.png')}
						resizeMode={FastImage.resizeMode.contain}
					/>
                )
			}
		},
		Profile: { screen: ProfileStackNavigation,
			navigationOptions:{
				tabBarLabel: "Profile",
                tabBarIcon: ({ focused, tintColor }) => (
					focused ? <FastImage
						style={{ width: 60, height: 60 }}
						source={require('../Assets/Image/profile_selected.png')}
						resizeMode={FastImage.resizeMode.contain}
					/>
					:
					<FastImage
						style={{ width: 60, height: 60 }}
						source={require('../Assets/Image/profile_normal.png')}
						resizeMode={FastImage.resizeMode.contain}
					/>
                )
			}
		},
	},
	{
		tabBarOptions: {
			activeTintColor: Colors.SECONDARY_COLOR,
			inactiveTintColor: Colors.SECONDARY_COLOR,
			labelStyle: {
				fontSize: 12,
			},
			style: {
				height: 70,
				backgroundColor: Colors.PRIMARY_COLOR,
				borderTopWidth: 2,
				borderColor: Colors.LIGHT_GREY,
				shadowColor: '#000',
				shadowRadius: 2,
				elevation: 5,
				shadowOpacity: 0.3,   
				shadowOffset: {
					width: 2,
					height: -2,
				},
			},
		},
	}
);