import React from 'react';
import {
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';
import {
    createStackNavigator,
    createMaterialTopTabNavigator,
} from 'react-navigation';
import FastImage from 'react-native-fast-image';
import FeatherIcon from 'react-native-vector-icons/Feather';

import Colors from '../Constants/Colors';

import ChatList from '../Screens/Chat/ChatList';
import ChatCreate from '../Screens/Chat/ChatCreate';
import ChatRoom from '../Screens/Chat/ChatRoom';
import ForwardMessage from '../Screens/Chat/ForwardMessage';
import NewGroupChat from '../Screens/Chat/NewGroupChat';
import GroupChatRoom from '../Screens/Chat/GroupChatRoom';

import Status from '../Screens/Chat/Status';
import Calls from '../Screens/Chat/Calls';

ChatTab = createMaterialTopTabNavigator(
    {
        ChatTab: { screen: ChatList, 
            navigationOptions:{
                tabBarLabel: "CHATS",
                tabBarIcon: ({ focused, tintColor }) => (
					focused ? <FastImage
						style={{ width: 50, height: 50 }}
						source={require('../Assets/Image/chats_selected.png')}
						resizeMode={FastImage.resizeMode.contain}
					/>
					:
					<FastImage
						style={{ width: 50, height: 50 }}
						source={require('../Assets/Image/chats_normal.png')}
						resizeMode={FastImage.resizeMode.contain}
					/>
                )
            }
        },
        StatusTab: { screen: Status, 
            navigationOptions:{
                tabBarLabel: "STATUS",
                tabBarIcon: ({ focused, tintColor }) => (
					focused ? <FastImage
						style={{ width: 50, height: 50 }}
						source={require('../Assets/Image/status_selected.png')}
						resizeMode={FastImage.resizeMode.contain}
					/>
					:
					<FastImage
						style={{ width: 50, height: 50 }}
						source={require('../Assets/Image/status_normal.png')}
						resizeMode={FastImage.resizeMode.contain}
					/>
                )
            }
        },
        CallsTab: { screen: Calls, 
            navigationOptions:{
                tabBarLabel: "CALLS",
                tabBarIcon: ({ focused, tintColor }) => (
					focused ? <FastImage
						style={{ width: 50, height: 50 }}
						source={require('../Assets/Image/calls_selected.png')}
						resizeMode={FastImage.resizeMode.contain}
					/>
					:
					<FastImage
						style={{ width: 50, height: 50 }}
						source={require('../Assets/Image/calls_normal.png')}
						resizeMode={FastImage.resizeMode.contain}
					/>
                )
            }
        },
    },
    {
        tabBarPosition: 'top',
        swipeEnabled: false,
        animationEnabled: true,
        tabBarOptions: {
            showIcon: true,
            activeTintColor: Colors.SECONDARY_COLOR,
            inactiveTintColor: Colors.SECONDARY_COLOR,
            style: {
                backgroundColor: Colors.PRIMARY_COLOR,
            },
            headerTintColor: '#000',
            labelStyle: {
                textAlign: 'center',
            },
            indicatorStyle: {
                borderColor: Colors.WHITE_COLOR,
                borderWidth: 2,
            },
            tabStyle:{
                flexDirection: 'row'
            },
        },
    }
);

const CustomHeader = props => {
    return (
        <View style={styles.container}>
            {props.haveBack ? 
            <TouchableOpacity style={{width:100, alignItems:'flex-start'}} onPress={() => props.navigation.goBack()}>
                <FeatherIcon name="arrow-left"  size={25} color="black" />
            </TouchableOpacity>
            :
            <View style={{width:100}} />
            }
            <View style={styles.markContainer}>
                <FastImage
                style={{ width: 60, height: 60 }}
                source={require('../Assets/Image/logo.png')}
                resizeMode={FastImage.resizeMode.contain} />
            </View>
            <View style={{width:100}} />
        </View>
    )
};

export default ChatStack = createStackNavigator(
    {
        ChatTab: { screen: ChatTab,
            navigationOptions:{
                header: props => <CustomHeader {...props} />,
            }
        },
        ChatCreate: { screen: ChatCreate,
            navigationOptions:{
                header: null,
            }
        },
        ChatRoom: { screen: ChatRoom,
            navigationOptions:{
                header: null,
            }
        },
        ForwardMessage: { screen: ForwardMessage,
            navigationOptions:{
                header: null,
            }
        },
        NewGroupChat: { screen: NewGroupChat,
            navigationOptions:{
                header: null,
            }
        },
        GroupChatRoom: { screen: GroupChatRoom,
            navigationOptions:{
                header: null,
            }
        },
    },
    {
        initialRouteName: 'ChatTab',
        defaultNavigationOptions: {
			gesturesEnabled: false,
            headerStyle: {
                backgroundColor: Colors.PRIMARY_COLOR,
            },
            headerTitleStyle: { 
                textAlign:"center", 
                flex:1 
            },
            headerTintColor: '#000',
        }
    }
);

ChatStack.navigationOptions = ({navigation}) => {
    let tabBarVisible = false;

    let routeName = navigation.state.routes[navigation.state.index].routeName;

    if(routeName === 'ChatTab'){
        tabBarVisible = true;
    }

    return {
        tabBarVisible
    };
}


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row', 
        height: 60, 
        alignItems: 'center', 
        justifyContent:'space-between',
        backgroundColor: Colors.PRIMARY_COLOR,
        // shadowColor: "#000",
        // shadowOffset: {
        //     width: 0,
        //     height: 3,
        // },
        // shadowOpacity: 0.3,
        // shadowRadius: 2,
        // elevation: 8,
        paddingHorizontal: 15,
    },

    title: {
        textAlign: 'center',
        color: Colors.BLACK_COLOR,
        fontSize: 20, 
    },

    markContainer: {
        flexDirection:'row', 
        alignItems:'center', 
        justifyContent: 'center',
    },

    largeMarkText: {
        textAlign: 'center',
        color: Colors.BLACK_COLOR,
        fontSize: 50, 
    },

    smallMarkText: {
        textAlign: 'center',
        color: Colors.BLACK_COLOR,
        fontSize: 15, 
        marginLeft: -10
    },

    rightButtonLabelText: {
        textAlign: 'center',
        color: Colors.PRIMARY_COLOR,
        fontSize: 18, 
    },

    leftButtonLabelText: {
        textAlign: 'center',
        color: Colors.PRIMARY_COLOR,
        fontSize: 18, 
    },
});