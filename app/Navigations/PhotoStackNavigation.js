import {
    createStackNavigator,
} from 'react-navigation';

import Photo from '../Screens/Photo/Photo';

export default PhotoStack = createStackNavigator(
    {
        Photo: { screen: Photo },
    },
    {
        initialRouteName: 'Photo',
        defaultNavigationOptions: {
			gesturesEnabled: false,
            headerStyle: {
                backgroundColor: '#42f44b',
            },
            headerTintColor: '#FFFFFF',
            title: 'Search',
        }
    }
);

PhotoStack.navigationOptions = ({navigation}) => {
    let tabBarVisible = false;

    let routeName = navigation.state.routes[navigation.state.index].routeName;

    if(routeName === 'Photo'){
        tabBarVisible = true;
    }

    return {
        tabBarVisible
    };
}