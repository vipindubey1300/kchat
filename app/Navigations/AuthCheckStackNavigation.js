import { createStackNavigator } from "react-navigation";

import AuthCheck from '../Screens/AuthCheck/AuthCheck';

export default AuthCheckStackNavigation = createStackNavigator({
		AuthCheck: AuthCheck,
	},
	{
		initialRouteName :'AuthCheck',
		defaultNavigationOptions: {
			gesturesEnabled: false,
		},
	}
);