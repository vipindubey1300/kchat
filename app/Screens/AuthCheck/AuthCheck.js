import React from 'react';
import {
	StyleSheet,
	View,
	ActivityIndicator,
	Alert,
} from 'react-native';
import firebase from 'react-native-firebase';
import DeviceInfo from 'react-native-device-info';

import { connect } from 'react-redux';
import { setUser, fetchDBProfile, updateDBUserDeviceID } from '../../Actions/UserActions';

class AuthCheck extends React.Component {
	static navigationOptions = ({navigation, screenProps}) => ({
		title: 'Sign Up',
		header: null,
	});

	constructor(props) {
		super(props);

		this.myDeviceId = DeviceInfo.getUniqueID();
		var user = firebase.auth().currentUser;
		if (user) {
			const uid = user.uid;
			this.props.setUser({
				uid: user.uid,
				phoneNumber: user.phoneNumber
			});

			new Promise((resolve, reject) => {
				this.props.fetchDBProfile(
					uid,
					resolve,
					reject,
				)
			})
			.then( result => {
				this.props.updateDBUserDeviceID(user.uid, this.myDeviceId);
				if(this.props.profile === null || this.props.profile.phone_number === undefined || this.props.profile.phone_number === null || this.props.profile.phone_number === ''
					|| this.props.profile.name === undefined || this.props.profile.name === null || this.props.profile.name === ''
					|| this.props.profile.photo_url === undefined || this.props.profile.photo_url === null || this.props.profile.photo_url === ''
					|| this.props.profile.address === undefined || this.props.profile.address === null || this.props.profile.address === '')
				{
					this.props.navigation.navigate('SignUpProfile', {
						phoneNumber: user.phoneNumber,
						uid: user.uid,
					});
				}
				else
					this.props.navigation.navigate('MainStackNavigation')
			})
			.catch( error => {
				console.log('Failed To Get Profile Information!')
				this.props.navigation.navigate('SignUpStackNavigation')
			})
		} else {
			console.log('Failed To Get Logged In User Information!')
			this.props.navigation.navigate('SignUpStackNavigation')
		}
	}

	componentDidMount() {
	}

	render() {
		return (
			<View style={styles.loadingContainer}>
				<ActivityIndicator size="large"/>
			</View>
		);
	}
}

const mapStateToProps = state => {
	return {
		profile : state.UserReducer.profile,
		user: state.UserReducer.user,
		isFetching: state.UserReducer.isFetching,
		isSuccess: state.UserReducer.isSuccess,
	};
};
  
const mapDispatchToProps = {
	setUser,
	fetchDBProfile,
	updateDBUserDeviceID,
};
  
export default connect(mapStateToProps, mapDispatchToProps)(AuthCheck);

const styles = StyleSheet.create({
    loadingContainer: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: '#fff',
	},	
});
