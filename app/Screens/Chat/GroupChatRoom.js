import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Dimensions,
    TouchableOpacity,
    Platform,
    Alert,
    Clipboard,
} from 'react-native';
import moment from 'moment';
import firebase from 'react-native-firebase';

import Dialog, {
    DialogContent,
    SlideAnimation,
} from 'react-native-popup-dialog';
import ActionSheet from 'react-native-action-sheet';
import ImagePicker from 'react-native-image-crop-picker';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import {GiftedChat, Bubble, Day, Avatar, Time } from 'react-native-gifted-chat';

import NavigationHeader from '../../Components/NavigationHeader'
import MessageImage from '../../Components/Chat/MessageImage'

import Colors from '../../Constants/Colors'
import { UPLOAD_CHAT_PHOTO } from '../../Utils/UploadUtil'
import { DOWNLOAD_PHOTO_FILE } from '../../Utils/DownloadUtil'
import { sendNotificationToUser } from '../../Utils/NotificationUtil';

import { connect } from 'react-redux';
import { fetchDBUserList } from '../../Actions/UserActions';

const { width, height } = Dimensions.get('window');
const MORE_MESSAGE_COUNT = 10;

function isSameDay(currentMessage, diffMessage) {
    if (!diffMessage.createdAt) {
        return false;
    }
    const currentCreatedAt = moment(currentMessage.createdAt);
    const diffCreatedAt = moment(diffMessage.createdAt);
    if (!currentCreatedAt.isValid() || !diffCreatedAt.isValid()) {
        return false;
    }
    return currentCreatedAt.isSame(diffCreatedAt, 'day');
}

function isSameUser(currentMessage, diffMessage) {
    return !!(diffMessage.user &&
        currentMessage.user &&
        diffMessage.user._id === currentMessage.user._id);
}

class GroupChatRoom extends React.Component {
	static navigationOptions = ({navigation, screenProps}) => ({
		title: 'Messages',
		header: null,
    });

	state = {
        messages: [],
        photoSelectDialogShow: false,
        loadEarlier: true,
        typingText: null,
        isLoadingEarlier: false,
	};

	constructor(props) {
        super(props);
        
        this._isMounted = false;
        this.onSendText = this.onSendText.bind(this);
        this.renderBubble = this.renderBubble.bind(this);
        this.onLoadEarlier = this.onLoadEarlier.bind(this);
        this.renderCustomActions = this.renderCustomActions.bind(this);
        this.renderMessageImage = this.renderMessageImage.bind(this);
        this.renderTime = this.renderTime.bind(this);
        this.onLongPress = this.onLongPress.bind(this);

        this.beginTimestamp = null;
        this.userInfoList = {};
    }
    
    componentWillMount() {
        const {navigation} = this.props;
        this.focusListener = navigation.addListener("willFocus", () => {
            this._initialize();
        });
    }

    componentWillUnmount() {
        firebase.database().ref('group_message/' + this.groupId).off('child_added');
        firebase.database().ref('group_message/' + this.groupId).off('child_changed');
        firebase.database().ref('group_message/' + this.groupId).off('child_removed');
        this.focusListener.remove();
    }

    async _initialize() {
        this._isMounted = true;
        this.groupId = this.props.navigation.getParam('groupId', '');
        this.members = this.props.navigation.getParam('members', '');
        
        firebase.database().ref('group_message/' + this.groupId).off('child_added');
        firebase.database().ref('group_message/' + this.groupId).off('child_changed');
        firebase.database().ref('group_message/' + this.groupId).off('child_removed');
        this.fetchGroupMessageList(this.groupId, this.updateMessageListCallback, this.appendMessageToFirstCallback);
    }

    fetchGroupUsersInfo = async () => {
        this.userInfoList = {};

        for(var i=0;i<this.members.length;i++){
            var member_uid = this.members[i];
            await firebase.database().ref('users/' + member_uid).once('value').then((userSnapshot) => {
                var userInfo = userSnapshot.val();
                this.userInfoList[member_uid] = userInfo;
            });
        }
    }

    fetchGroupMessageList = async (groupId, updateMessageListCallback, appendMessageToFirstCallback) => {
        const { user } = this.props;
        const myUid = user.uid;   
        await this.fetchGroupUsersInfo();
        var setMessageReceivedStatus = this.setMessageReceivedStatus;
        var changeInMessage = this.changeInMessage;
        var deleteForAllMessage = this.deleteForAllMessage;
        const userInfoList = this.userInfoList;

        var lastTimeStamp = 0;
        await firebase.database().ref('group_message/' + groupId).orderByChild('timestamp').limitToLast(MORE_MESSAGE_COUNT).once('value', function(snapshot) {
            var groupMessageList = [];
            var messageCount = 0;
            snapshot.forEach(function(subSnapshot) {
                var messageInfo = subSnapshot.val();
                messageCount++;

                const sender_id = messageInfo.user_uid;
                const hide = messageInfo.hide;
                if(sender_id === myUid && hide === true) return;

                lastTimeStamp = messageInfo.timestamp;
                var type = messageInfo.type ? messageInfo.type : 'text';
                var giftMessage = {
                    _id: Math.round(Math.random() * 1000000),
                    createdAt: new Date(messageInfo.timestamp),
                    user: {
                        _id: messageInfo.user_uid,
                        name: userInfoList[messageInfo.user_uid].name,
                        avatar: userInfoList[messageInfo.user_uid].photo_url,
                    },
                    key: subSnapshot.key,
                    sent: messageInfo.sent ? messageInfo.sent : false,
                    received: messageInfo.received ? messageInfo.received : false,
                };
                // type : text, image
                giftMessage[type] = messageInfo.message;
                groupMessageList.unshift(giftMessage);

                // // if recv_uid is mine, set message status to received
                // const recv_uid = messageInfo.recv_uid;
                // setMessageReceivedStatus(groupId, subSnapshot.key, recv_uid);
            });
            updateMessageListCallback(groupMessageList, messageCount);
        });
    
        firebase.database().ref('group_message/' + groupId).orderByChild('timestamp').startAt(lastTimeStamp + 1).on('child_added', function(snapshot) {
            var messageInfo = snapshot.val();
            var type = messageInfo.type ? messageInfo.type : 'text';
            var giftMessage = {
                _id: Math.round(Math.random() * 1000000),
                createdAt: new Date(messageInfo.timestamp),
                user: {
                    _id: messageInfo.user_uid,
                    name: userInfoList[messageInfo.user_uid].name,
                    avatar: userInfoList[messageInfo.user_uid].photo_url,
                },
                key: snapshot.key,
                sent: true,
            };

            // // if recv_uid is mine, set message status to received
            // const recv_uid = messageInfo.recv_uid;
            // setMessageReceivedStatus(groupId, snapshot.key, recv_uid);

            // type : text, image, video
            giftMessage[type] = messageInfo.message;
            appendMessageToFirstCallback(giftMessage);
        });

        firebase.database().ref('group_message/' + groupId).orderByChild('timestamp').limitToLast(MORE_MESSAGE_COUNT).on('child_changed', function(data) {
            changeInMessage(data.key, data.val());
        });

        firebase.database().ref('group_message/' + groupId).orderByChild('timestamp').startAt(lastTimeStamp + 1).on('child_changed', function(data) {
            changeInMessage(data.key, data.val());
        });

        firebase.database().ref('group_message/' + groupId).on('child_removed', function(data) {
            deleteForAllMessage(data.key, data.val());
        });
    }

    setMessageReceivedStatus = (groupId, key, recv_uid) => {
        // const { user } = this.props;
        // if(user.uid === recv_uid)
        //     firebase.database().ref('group_message/' + groupId + '/' + key).update({received:true});
    }

    deleteForAllMessage = (key, data) => {
        var { messages } = this.state;
        var foundIndex = messages.findIndex(message => message.key === key);
        if(foundIndex === -1) return;
        messages.splice(foundIndex,1);
        this.setState({messages : messages.slice()})
    }

    changeInMessage = (key, data) => {
        const { user } = this.props;
        const myUid = user.uid;
        const sender_id = data['user_uid'];
        var { messages } = this.state;
        var foundIndex = messages.findIndex(message => message.key === key);
        if(foundIndex === -1) return;

        // change sent, received status
        var message = Object.assign({}, messages[foundIndex]);
        message['sent'] = data['sent'] ? data['sent'] : false;
        message['received'] = data['received'] ? data['received'] : false;
        messages[foundIndex] = message;
        
        // check if delete for self
        if(myUid === sender_id && data['hide'] === true){
            messages.splice(foundIndex, 1);
        }
        this.setState({messages : messages.slice()})
    }

  
    appendMessageToFirstCallback = (message) => {
        var newMessages = [message];
        
        if (this._isMounted === true) {
            this.setState({messages : newMessages.concat(this.state.messages)})
        }
    }

    appendMessagesToLastCallback = (newMessages, count) => {
        // Update the displayed message's begin for view more message history
        if(newMessages.length > 0)
            this.beginTimestamp = new Date(newMessages[newMessages.length-1].createdAt).getTime();

        if(count < MORE_MESSAGE_COUNT)
            this.removeLoadEarilerButton();
            
        if (this._isMounted === true) {
            this.setState({messages : this.state.messages.concat(newMessages)})
        }
    }

    updateMessageListCallback = (messages, count) => {
        // Get the displayed message's begin for view more message history
        if(messages.length > 0)
            this.beginTimestamp = new Date(messages[messages.length-1].createdAt).getTime();
        
        if(count < MORE_MESSAGE_COUNT)
            this.removeLoadEarilerButton();

        if (this._isMounted === true) {
            this.setState({messages})
        }
    }

    removeLoadEarilerButton = () => {
        this.setState({loadEarlier:false});
    }

    onLoadEarlier(appendMessagesToLastCallback) {
        const { user } = this.props;
        const myUid = user.uid;   
        var changeInMessage = this.changeInMessage;
        var setMessageReceivedStatus = this.setMessageReceivedStatus;
        const userInfoList = this.userInfoList;
        const groupId = this.groupId;
        firebase.database().ref('group_message/' + groupId).orderByChild('timestamp').endAt(this.beginTimestamp - 1).limitToLast(MORE_MESSAGE_COUNT).once('value', function(snapshot) {
            var groupMessageList = [];
            var messageCount = 0;
            snapshot.forEach(function(subSnapshot) {
                messageCount++;
                var messageInfo = subSnapshot.val();
                
                const sender_id = messageInfo.user_uid;
                const hide = messageInfo.hide;
                if(sender_id === myUid && hide === true) return;

                var type = messageInfo.type ? messageInfo.type : 'text';
                var giftMessage ={
                    _id: Math.round(Math.random() * 1000000),
                    createdAt: new Date(messageInfo.timestamp),
                    user: {
                        _id: messageInfo.user_uid,
                        name: userInfoList[messageInfo.user_uid].name,
                        avatar: userInfoList[messageInfo.user_uid].photo_url,
                    },
                    key: subSnapshot.key,
                    sent: messageInfo.sent ? messageInfo.sent : false,
                    received: messageInfo.received ? messageInfo.received : false,
                };
                // type : text, image
                giftMessage[type] = messageInfo.message;
                groupMessageList.unshift(giftMessage);

                // // if recv_uid is mine, set message status to received
                // const recv_uid = messageInfo.recv_uid;
                // setMessageReceivedStatus(groupId, subSnapshot.key, recv_uid);    

            });
            appendMessagesToLastCallback(groupMessageList, messageCount);
        });

        firebase.database().ref('group_message/' + groupId).orderByChild('timestamp').endAt(this.beginTimestamp - 1).limitToLast(MORE_MESSAGE_COUNT).on('child_changed', function(data) {
            changeInMessage(data.key, data.val());
        });
    }

    insertGroupMessageRecord = (newMessageInfo, groupId) => {
        // insert new message to group_message table and return server timestamp value
        var newMessageRef = firebase.database().ref('group_message/' + groupId).push();
        return new Promise((resolve, reject) => {
            newMessageRef.set(newMessageInfo)
            .then(() => {
                return firebase.database().ref('group_message/' + groupId + '/' + newMessageRef.key + '/timestamp').once('value')
                .then((snapshot) => {
                    serverTimestamp = snapshot.val();
                    resolve(serverTimestamp);
                })
                .catch(() => {
                    reject(0);
                });
            })
            .catch(() => {
                reject(0);
            })
        })
    }

    sendGroupMessage = async (newMessageInfo, groupId, myUID, type, message) => {
        const timestamp = await this.insertGroupMessageRecord(newMessageInfo, groupId);
        
        if(timestamp == 0) return;
        
        // update with the same timestamp
        var messageInfo = {
            timestamp : timestamp,
            message : message,
            type: type,
            user_uid : myUID,
        };
        firebase.database().ref('chat_group/' + groupId + '/last_message_info').set(messageInfo)
        .then(function() {})
        .catch(function(error) {
            console.log("Error saving group last message info into chat_group table: ", error);
        });        
    }

    onSendText(messages = []) {
        if (messages.length > 0) {
            var newMessageInfo = {
                timestamp : firebase.database.ServerValue.TIMESTAMP,
                message : messages[0].text,
                type: "text",
                user_uid : this.props.user.uid,
                sent: true,
            };
            
            var myUID = this.props.user.uid;
            var groupId = this.groupId;
            var message = messages[0].text;
            
            this.sendGroupMessage(newMessageInfo, groupId, myUID, "text", message);

            for(var i=0;i<this.members.length;i++) {
                    sendNotificationToUser(this.members[i], {
                    "notification": {
                        "title": "New Group Message from " + (this.userInfoList[myUID].name ? this.userInfoList[myUID].name : 'someone'),
                        "body": message,
                        "sound": "default",
                        "click_action" : ".MainActivity"
                    },
                    "priority": "high"
                });
            }
        }
    }
    
    renderBubble(props) {
        if (isSameUser(props.currentMessage, props.previousMessage) && isSameDay(props.currentMessage, props.previousMessage)) {
            return (
            <Bubble
                {...props}
                textStyle={{
                    left: {
                        color: Colors.BLACK_COLOR,
                    },
                    right: {
                        color: Colors.WHITE_COLOR,
                    }
                }}
                wrapperStyle={{
                    left: {
                        marginLeft: 8,
                        backgroundColor: '#e2edf9',
                    },
                    right: {
                        backgroundColor: '#000',
                    }
                }}
                tickStyle = {{
                    color: '#3fc3e9',
                }}
            />
            );
        }
        return (
            <View>
                {props.position === 'left' && <Text style={{textAlign:props.position}}>{props.currentMessage.user.name}</Text>}
                <Bubble
                    {...props}
                    textStyle={{
                        left: {
                            color: Colors.BLACK_COLOR,
                        },
                        right: {
                            color: Colors.WHITE_COLOR,
                        }
                    }}
                    wrapperStyle={{
                        left: {
                            backgroundColor: '#e2edf9',
                        },
                        right: {
                            backgroundColor: '#000',
                        }
                    }}
                    tickStyle = {{
                        color: '#3fc3e9',
                    }}
                />
            </View>
        );
    }

    renderTime(props) {
        return (
            <Time
                {...props}
                textStyle={{
                    left: {
                        color: Colors.BLACK_COLOR,
                    },
                    right: {
                        color: Colors.WHITE_COLOR,
                    }
                }}
            />
        );
    }

    renderMessageImage(props){
        return <MessageImage {...props} />
    }

    renderDay(props) {
        return <Day {...props} textStyle={{color: 'black', fontSize:20}} 
            containerStyle={{flex:1, justifyContent: 'flex-start',alignItems: 'flex-start', margin: 10, borderBottomColor:'#aaa', borderBottomWidth:1}}
            wrapperStyle={{}}/>
    }

    renderAvatar(props) {
        return <Avatar {...props} imageStyle={{
            left: {
                height: 36,
                width: 36,
                borderRadius: 18,
            },
        }}/>;
    }
    
    onPressAvatar(props){
        console.log("Avator Pressed.")
    }

    onSendPhoto = () => {
        this.setState({photoSelectDialogShow:true});
    }

    _onSendPhoto = (url) => {    
        var newMessageInfo = {
            timestamp : firebase.database.ServerValue.TIMESTAMP,
            message : url,
            type: "image",
            user_uid : this.props.user.uid,
            sent: true,
        };
        
        var myUID = this.props.user.uid;
        var groupId = this.groupId;
        var message = url;
        
        this.sendGroupMessage(newMessageInfo, groupId, myUID, "image", message);

        for(var i=0;i<this.members.length;i++) {
            sendNotificationToUser(this.members[i], {
                "notification": {
                    "title": "New Group Message from " + (this.userInfoList[myUID].name ? this.userInfoList[myUID].name : 'someone'),
                    "body": "[Photo message]",
                    "sound": "default",
                    "click_action" : ".MainActivity"
                },
                "priority": "high"
            });
        }
    }

    pickPhotoFromCamera() {
        const { user } = this.props;
        ImagePicker.openCamera({
            mediaType: "photo",
        }).then(async(image) => {
            var photo = await UPLOAD_CHAT_PHOTO(image.path, user.uid, 'chat', 'image/jpg');
            this._onSendPhoto(photo);
        }).catch(e => {
            console.log(e);
            Alert.alert(e.message ? e.message : e);
        });
    }

    pickPhotoFromGallery() {
        const { user } = this.props;
        ImagePicker.openPicker({
            mediaType: "photo",
        }).then(async(image) => {
            var photo = await UPLOAD_CHAT_PHOTO(image.path, user.uid, 'chat', 'image/jpg');
            this._onSendPhoto(photo);
        }).catch(e => {
            console.log(e);
            Alert.alert(e.message ? e.message : e);
        });
    }

    onSendVoice = () => {

    }

    renderCustomActions(props) {
        return (
            <View style={{flexDirection:'row', alignSelf:'center'}}>
                <TouchableOpacity style={{marginLeft:10, justifyContent:'center', alignItems:'center', width:30, height:30, borderRadius:15, backgroundColor:Colors.PRIMARY_COLOR}} onPress={() => this.onSendPhoto()}>
                    <FontAwesomeIcon name="paperclip" size={20} color={Colors.WHITE_COLOR} />
                </TouchableOpacity>
                {/* <TouchableOpacity style={{marginLeft:10, justifyContent:'center', alignItems:'center', width:30, height:30, borderRadius:15, backgroundColor:Colors.PRIMARY_COLOR}} onPress={() => this.onSendVoice()}>
                    <FontAwesomeIcon name="microphone" size={20} color={Colors.WHITE_COLOR} />
                </TouchableOpacity> */}
            </View>
        )
    }

    onLongPress(context, currentMessage) {
        this.showActionSheet(currentMessage);
    }
    
    handleImageLongPress = (currentMessage) => {
        this.showActionSheet(currentMessage);
    }

    showActionSheet = (currentMessage) => {
        const { user } = this.props;
        var senderInfo = currentMessage.user;
        if(senderInfo === null || senderInfo === undefined)
            return;

        var sender_id = senderInfo._id;
        let isMyAction = false;
        if(sender_id == user.uid) isMyAction = true;

        var messageType;
        if(currentMessage.hasOwnProperty('image')) {
            messageType = 'image'
        }
        else if(currentMessage.hasOwnProperty('text')) {
            messageType = 'text'
        }
           
        var buttons = [
            'Forward',
        ];

        if(messageType === "text")
            buttons.push('Copy')
        else
            buttons.push('Download')

        if(isMyAction){
            buttons.push('Delete for Self');
            buttons.push('Delete for All');
        }
           
        var DESTRUCTIVE_INDEX = isMyAction ? 4 : 2;
        var CANCEL_INDEX = isMyAction ? 5 : 3;
           
        ActionSheet.showActionSheetWithOptions({
            options: buttons,
            cancelButtonIndex: CANCEL_INDEX,
            destructiveButtonIndex: DESTRUCTIVE_INDEX,
            tintColor: 'blue'
        },
        (buttonIndex) => {
            console.log('button clicked :', buttonIndex);
            if(buttonIndex === 0){  //forward
                this.props.navigation.navigate('ForwardMessage', {
                    message: currentMessage,
                })
            }
            else if(buttonIndex === 1){
                if(messageType === 'text') {    // copy
                    Clipboard.setString(currentMessage.text);
                }
                else if(messageType === 'image') {  //download
                    DOWNLOAD_PHOTO_FILE(currentMessage.image);
                }
            }
            else if(isMyAction && buttonIndex === 2){   // delete for self
                firebase.database().ref('group_message/' + this.groupId + '/' + currentMessage.key).update({
                    hide: true,
                });
            }
            else if(isMyAction && buttonIndex === 3){   // delete for all
                firebase.database().ref('group_message/' + this.groupId + '/' + currentMessage.key).remove();
            }
        });
    }

	render() {
        const { user } = this.props;
        const { messages, loadEarlier } = this.state;
		return (
			<View style={styles.mainContainer}>
				<View>
					<NavigationHeader haveBack={true} title="Messages"/>
				</View>
				<View style={{flex:1}}>
                    <GiftedChat
                        messages={messages}
                        onSend={this.onSendText}
                        onLongPress={this.onLongPress}
                        loadEarlier={loadEarlier}
                        onLoadEarlier={() => this.onLoadEarlier(this.appendMessagesToLastCallback)}
                        isLoadingEarlier={this.state.isLoadingEarlier}
                        onPressAvatar ={this.onPressAvatar.bind(this)}
                        renderAvatarOnTop={true}
                        renderActions={this.renderCustomActions}
                        renderMessageImage={this.renderMessageImage}
                        lightboxProps={{
                            onLongPress : (currentMessage) => this.handleImageLongPress(currentMessage)
                        }}
                        placeholder="Write your message..."

                        user={{
                            _id: user.uid,
                        }}

                        textInputProps={
                            style = {
                                borderWidth:1, 
                                borderRadius:18, 
                                fontSize:10, 

                                paddingHorizontal: 15,
                                paddingVertical: 0,
                                fontSize: 16,
                                lineHeight: 20,
                                marginTop: Platform.select({
                                    ios: 6,
                                    android: 0,
                                }),
                                marginBottom: Platform.select({
                                    ios: 5,
                                    android: 3,
                                }),
                                marginRight: 10,
                            }
                        }

                        renderBubble={this.renderBubble}
                        renderTime={this.renderTime}
                        renderDay={this.renderDay}
                        renderAvatar={this.renderAvatar.bind(this)}
                    />
                    <Dialog
                        onDismiss={() => this.setState({ photoSelectDialogShow: false })}
                        onTouchOutside={() => this.setState({ photoSelectDialogShow: false })}
                        onHardwareBackPress={() => this.setState({ photoSelectDialogShow: false })}
                        rounded={false}
                        width={0.9}
                        visible={this.state.photoSelectDialogShow}
                        dialogAnimation={new SlideAnimation({
                            slideFrom: 'bottom',
                        })}>
                        <DialogContent
                            style={{backgroundColor: 'white',}}>
                            <View style={{flexDirection:'column', alignItems: 'center', marginTop:20,}}>              
                                <Text style={{fontSize:18, color: Colors.BLACK_COLOR, marginBottom: 10,}}>Select Photo from</Text>
                                <View style={styles.dialogButtonGroupStyle}>
                                    <TouchableOpacity style={styles.dialogButtonStyle}
                                    onPress={() => {this.setState({ photoSelectDialogShow: false }); this.pickPhotoFromGallery();}}>
                                        <Text style={{fontSize:16,color:'black',textAlign:'center',}}>From Gallery</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.dialogButtonStyle}
                                    onPress={() => {this.setState({ photoSelectDialogShow: false }); this.pickPhotoFromCamera();}}>
                                        <Text style={{fontSize:16,color:'black',textAlign:'center',}}>From Camera</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </DialogContent>
                    </Dialog>
				</View>
			</View>
		);
	}
}

const mapStateToProps = state => {
	return {
		user : state.UserReducer.user,
	};
};
  
const mapDispatchToProps = {
	fetchDBUserList,
};
  
export default connect(mapStateToProps, mapDispatchToProps)(GroupChatRoom);

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
	},
		
    avatar: {
        width: 70,
		height: 70,
		marginRight: 15,
        borderRadius: 35,
    },

	seperator: {
		marginVertical: 1, 
		height: 2, 
		backgroundColor: Colors.LIGHT_GREY,
	},

    dialogButtonGroupStyle: {
		flexDirection: 'row',
		marginTop: 10,
		height: 50,
		alignItems: 'center',
    },
  
    dialogButtonStyle: {
		height: 50, 
		width: '45%',
		justifyContent: 'center', 
		alignItems: 'center',
		backgroundColor: 'white', 
		borderColor: 'gray', 
		borderWidth: 1, 
		marginRight: 10,
    },
});
