import React from 'react';
import {
	StyleSheet,
	Text,
	View,
	Alert,
	FlatList,
	Dimensions,
	ActivityIndicator,
	TouchableOpacity,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import ImagePicker from 'react-native-image-crop-picker';
import firebase from 'react-native-firebase';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome'
import Dialog, {
    DialogContent,
    SlideAnimation,
} from 'react-native-popup-dialog';

import NavigationHeader from '../../Components/NavigationHeader'
import UnderlineTextInput from '../../Components/TextInput/UnderlineTextInput';
import Colors from '../../Constants/Colors';
import KeyboardView from '../../Components/KeyboardView';
import { UPLOAD_AVATAR } from '../../Utils/UploadUtil'

import { connect } from 'react-redux';
import { fetchDBUserList } from '../../Actions/UserActions';

const { width, height } = Dimensions.get('window');

class NewGroupChat extends React.Component {
	
	state = {
		loading: false,
		photoSelectDialogShow: false,
		photo: "https://image.flaticon.com/icons/png/512/36/36567.png",
		name: null,
		userList: [],
		refresh: false,
	};

	constructor(props) {
		super();
	}

	componentDidMount() {
		new Promise((resolve, reject) => {
			this.props.fetchDBUserList(
				resolve,
				reject,
			)
		})
		.then( success => {
			console.log("Successfully Fetched User List!");
			this.setState({userList: this.props.userList})
		})
		.catch( error => {
			console.log(error)
		})
	}


	onSaveClick = async () => {	
		const { photo, userList } = this.state;
		const { user } = this.props;
		const name = this.nameInput.getInputValue();

		if(photo === null ){
			Alert.alert("Error", 'Please Select a Photo!');
			return;
		}

		if(name === '' ){
			Alert.alert("Error", 'Please Enter a Name!');
			return;
		}

		this.setState({loading:true});

		var members = {};
		members[`${user.uid}`] = user.uid;
		for(var i=0;i<userList.length;i++){
			if(userList[i]['added'] === true){
				members[`${userList[i].uid}`] = userList[i].uid;
			}
		}

		// If not exist create a new one
		var newChatGroupRef = await firebase.database().ref('chat_group').push();
		var photo_url = photo;
		if(!photo.startsWith("http"))
			photo_url = await UPLOAD_AVATAR(photo_url, newChatGroupRef.key, 'group', 'image/jpg');
        var newChatGroupInfo = {
			ctime : firebase.database.ServerValue.TIMESTAMP,
			group_name: name,
			photo_url: photo_url,
			members: members,
            group_id: newChatGroupRef.key,
        };
        await newChatGroupRef.set(newChatGroupInfo);
		this.setState({loading:false});
		this.props.navigation.pop();
		this.props.navigation.navigate('ChatTab');
	}

	onAddPhotoClick = () => {
        this.setState({photoSelectDialogShow:true});
	}

    pickSingleFromCamera = () => {
        ImagePicker.openCamera({
            cropping: true,
            width: 300,
            height: 300,
            includeExif: true,
        }).then(async (image) => {
			this.setState({photo: image.path});
        }).catch(e => {
            console.log(e);
        });
    }

    pickSingleFromGallery = () => {
        ImagePicker.openPicker({
            width: 300,
            height: 300,
            cropping: true,
            includeExif: true,
        }).then(async (image) => {
			this.setState({photo: image.path});
        }).catch(e => {
            console.log(e);
        });
	}

	toggleMember = async (userInfo) => {
		const uid = userInfo.uid;
		var status = userInfo['added'];
		if(status === true)
			userInfo['added'] = false;
		else
			userInfo['added'] = true;
		this.setState({refresh: !this.state.refresh});
	}

	_RenderItem = (item, index) => {
		if(item.uid === this.props.user.uid) return;
		return (
			<View style={styles.chatContainer}>
				<FastImage
                    style={styles.avatar}
                    source={{
                        uri: item.photo_url,
                        priority: FastImage.priority.high,
                    }}
                    resizeMode={FastImage.resizeMode.contain}
                />
				<View style={styles.chatSubContainer}>
					<Text style={styles.nameText}>{item.name}</Text>
					<Text style={styles.messageText} numberOfLines={2}>{item.address}</Text>
				</View>				
				<TouchableOpacity style={{alignItems: 'center', width: 120}} onPress={() => this.toggleMember(item)}>
					{ item['added'] ?
					<View style={styles.removeButton}>
						<Text style={styles.removeButtonText}>Added</Text>
					</View>
					:
					<View style={styles.addButton}>
						<Text style={styles.addButtonText}>Add</Text>
					</View>
					}
				</TouchableOpacity>
			</View>
		)
	  }

	render() {
		const { isFetching } = this.props;
        const { loading, photo, userList, refresh } = this.state;

        if (loading || isFetching)
            return (
                <View style={styles.loadingContainer}>
                    <ActivityIndicator size="large"/>
                </View>
			);
			
		return (
			<View style={styles.mainContainer}>
				<View>
					<NavigationHeader haveBack={true} title="New Group Chat" rightButtonLabel="Save" rightButtonAction={this.onSaveClick.bind(this)}/>
				</View>
				<View style={{alignSelf: 'center', marginTop: 20}} >
					<TouchableOpacity style={styles.takePhotoButton} onPress={() => this.onAddPhotoClick()}>
						<FontAwesomeIcon name="instagram" size={25} color={Colors.WHITE_COLOR} style={{textAlign:'center'}}/>
					</TouchableOpacity>
					<FastImage
						style={styles.photo}
						source={{
							uri: photo,
							priority: FastImage.priority.normal,
						}}
						resizeMode={FastImage.resizeMode.contain}
					/>
				</View>
				<UnderlineTextInput
					placeholder="Group Name"
					keyboardType="default"
					style={styles.nameInput}
					error={false}
					// focus={this.onChangeInputFocus}
					ref={ref => this.nameInput = ref}
					returnKeyType="done"
				/>
				<View style={{flex:1, alignItems:'center', marginTop: 30,}}>
					<Text style={styles.descriptionText}>Please Select Chat Group Members.</Text>
					<FlatList
						data={userList}
						extraData={refresh}
						keyExtractor={(item, index) => item.uid}
						renderItem={({item, index}) => this._RenderItem(item, index)}
					/>
				</View>
				{/* Photo Selection Dialog */}
                <Dialog
                    onDismiss={() => this.setState({ photoSelectDialogShow: false })}
                    onTouchOutside={() => this.setState({ photoSelectDialogShow: false })}
                    onHardwareBackPress={() => this.setState({ photoSelectDialogShow: false })}
                    rounded={false}
                    width={0.9}
                    visible={this.state.photoSelectDialogShow}
                    dialogAnimation={new SlideAnimation({
                        slideFrom: 'bottom',
                    })}>
                    <DialogContent
                        style={{backgroundColor: 'white',}}>
                        <View style={{flexDirection:'column', alignItems: 'center', marginTop:20,}}>              
                            <Text style={{fontSize:18, color: Colors.BLACK_COLOR, marginBottom: 10,}}>Select Photo from</Text>
                            <View style={styles.dialogButtonGroupStyle}>
                                <TouchableOpacity style={styles.dialogButtonStyle}
                                onPress={() => {this.setState({ photoSelectDialogShow: false }); this.pickSingleFromGallery();}}>
                                    <Text style={{fontSize:16,color:'black',textAlign:'center',}}>From Gallery</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.dialogButtonStyle}
                                onPress={() => {this.setState({ photoSelectDialogShow: false }); this.pickSingleFromCamera();}}>
                                    <Text style={{fontSize:16,color:'black',textAlign:'center',}}>From Camera</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </DialogContent>
                </Dialog>
			</View>
		);
	}
}

const mapStateToProps = state => {
	return {
		user : state.UserReducer.user,
		userList : state.UserReducer.userList,
		isFetching: state.UserReducer.isFetching,
		isSuccess: state.UserReducer.isSuccess,
	};
};
  
const mapDispatchToProps = {
	fetchDBUserList,
};
  
export default connect(mapStateToProps, mapDispatchToProps)(NewGroupChat);

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
	},

    loadingContainer: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: '#fff',
	},

	chatContainer: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		marginHorizontal: 10, 
		marginVertical: 15, 
	},

	chatSubContainer: {
		width: width - 220,
		flexDirection: 'column',
		alignItems: 'flex-start',
		marginLeft: 20,
	},

	avatar: {
		width: 70,
		height: 70,
		borderRadius: 35,
	},
	
	nameText: {
		fontSize: 18, 
		color: Colors.BLACK_COLOR,
	},
	
	messageText: {
		fontSize: 14, 
		color: Colors.GREY,
	},
	
	descriptionText: {
		textAlign: 'center', 
		width: '70%', 
		fontSize: 16, 
		color: Colors.BLACK_COLOR,
	},

	photo: {
		width: 150,
		height: 150,
		borderRadius: 75, 
		borderWidth: 1,
		borderColor: Colors.BLACK_COLOR,
		zIndex: 1,
	},

	nameInput: {
		marginHorizontal: 40,
		paddingTop: 0,
		marginTop: 0,
	},

	takePhotoButton: {
		width: 40, 
		height: 40, 
		position: 'absolute',
		backgroundColor:'rgba(0,0,0,0.5)',
		borderRadius: 20,
		justifyContent: 'center',
		top: 70,
		bottom: 0,
		left: 130,
		right: 0,
		zIndex: 2,
	},

    dialogButtonGroupStyle: {
		flexDirection: 'row',
		marginTop: 10,
		height: 50,
		alignItems: 'center',
    },
  
    dialogButtonStyle: {
		height: 50, 
		width: '45%',
		justifyContent: 'center', 
		alignItems: 'center',
		backgroundColor: 'white', 
		borderColor: 'gray', 
		borderWidth: 1, 
		marginRight: 10,
    },

	removeButton: {
		backgroundColor: '#fff', 
		borderRadius: 20,
		borderWidth: 1,
		borderColor: Colors.PRIMARY_COLOR,
	},
	
	removeButtonText: {
		marginVertical: 10,
		marginHorizontal: 20,
		fontSize: 16, 
		color: Colors.PRIMARY_COLOR,
	},

	addButton: {
		backgroundColor: Colors.PRIMARY_COLOR, 
		borderRadius: 20,
	},

	addButtonText: {
		marginVertical: 10,
		marginHorizontal: 20,
		fontSize: 16, 
		color: '#fff',
	},
});
