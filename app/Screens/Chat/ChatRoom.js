import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Dimensions,
    TouchableOpacity,
    Platform,
    Alert,
    Clipboard,
} from 'react-native';
import moment from 'moment';
import firebase from 'react-native-firebase';

import Dialog, {
    DialogContent,
    SlideAnimation,
} from 'react-native-popup-dialog';
import ActionSheet from 'react-native-action-sheet';
import ImagePicker from 'react-native-image-crop-picker';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import {GiftedChat, Bubble, Day, Avatar, Time } from 'react-native-gifted-chat';

import NavigationHeader from '../../Components/NavigationHeader'
import MessageImage from '../../Components/Chat/MessageImage'
import Message from '../../Components/Chat/Message'

import Colors from '../../Constants/Colors'
import { UPLOAD_CHAT_PHOTO } from '../../Utils/UploadUtil'
import { DOWNLOAD_PHOTO_FILE } from '../../Utils/DownloadUtil'
import { sendNotificationToUser } from '../../Utils/NotificationUtil';

import { connect } from 'react-redux';
import { fetchDBUserList } from '../../Actions/UserActions';

const { width, height } = Dimensions.get('window');
const MORE_MESSAGE_COUNT = 10;

function isSameDay(currentMessage, diffMessage) {
    if (!diffMessage.createdAt) {
        return false;
    }
    const currentCreatedAt = moment(currentMessage.createdAt);
    const diffCreatedAt = moment(diffMessage.createdAt);
    if (!currentCreatedAt.isValid() || !diffCreatedAt.isValid()) {
        return false;
    }
    return currentCreatedAt.isSame(diffCreatedAt, 'day');
}

function isSameUser(currentMessage, diffMessage) {
    return !!(diffMessage.user &&
        currentMessage.user &&
        diffMessage.user._id === currentMessage.user._id);
}

class ChatRoom extends React.Component {
	static navigationOptions = ({navigation, screenProps}) => ({
		title: 'Messages',
		header: null,
    });

	state = {
        messages: [],
        photoSelectDialogShow: false,
        loadEarlier: true,
        typingText: null,
        isLoadingEarlier: false,
	};

	constructor(props) {
        super(props);
        
        this._isMounted = false;
        this.onSendText = this.onSendText.bind(this);
        this.renderBubble = this.renderBubble.bind(this);
        this.onLoadEarlier = this.onLoadEarlier.bind(this);
        this.renderCustomActions = this.renderCustomActions.bind(this);
        this.renderMessage = this.renderMessage.bind(this);
        this.renderMessageImage = this.renderMessageImage.bind(this);
        this.renderTime = this.renderTime.bind(this);
        this.onLongPress = this.onLongPress.bind(this);

        this.beginTimestamp = null;
        this.userInfoList = {};

        // update last message
        this.haveToUpdateOpponentLastMessageInfo = false;
        this.haveToUpdateMyLastMessageInfo = false;

    }
    
    componentWillMount() {
        const {navigation} = this.props;
        this.focusListener = navigation.addListener("willFocus", () => {
            this._initialize();
        });
    }

    componentWillUnmount() {
        // firebase.database().ref('users/' + this.opponentUid).off('value');

        firebase.database().ref('pair_message/' + this.chatPairId).off('child_added');
        firebase.database().ref('pair_message/' + this.chatPairId).off('child_changed');
        firebase.database().ref('pair_message/' + this.chatPairId).off('child_removed');
        this.focusListener.remove();
    }

    async _initialize() {
        this._isMounted = true;
        this.chatPairId = this.props.navigation.getParam('pairId', '');
        this.opponentUid = this.props.navigation.getParam('opponentUid', '');
        var updateOldMessageUserInfo = this.updateOldMessageUserInfo;
        
        if(this.chatPairId === '')  //opens a new chat room
            await this.openNewChatPair();

        // firebase.database().ref('users/' + this.opponentUid).off('value');
        firebase.database().ref('users/' + this.opponentUid).on('value', userSnapshot => {
            var userInfo = userSnapshot.val();
            this.userInfoList[this.opponentUid] = userInfo;
            updateOldMessageUserInfo(userInfo);
        });
        
        firebase.database().ref('pair_message/' + this.chatPairId).off('child_added');
        firebase.database().ref('pair_message/' + this.chatPairId).off('child_changed');
        firebase.database().ref('pair_message/' + this.chatPairId).off('child_removed');
        this.fetchPairMessageList(this.chatPairId, this.updateMessageListCallback, this.appendMessageToFirstCallback);
    }

    updateOldMessageUserInfo = (userInfo) => {
        var { messages } = this.state;
        for(var i=0;i<messages.length;i++){
            var message = messages[i];
            var origUserId = message.user._id;
            if(origUserId === this.opponentUid) {
                message.user.name = userInfo.name;
                message.user.avatar = userInfo.photo_url;
            }
        }
        this.setState({messages : messages.slice()})
    }

    openNewChatPair = async() => {
        if(!this.props.user) return;

        const uid = this.props.user.uid;

        // Check if the chat pair already exist
        var bExist = false;
        var existChatPairId = '';
        await firebase.database().ref('chat_pair/' + uid).orderByChild('recv_uid').equalTo(this.opponentUid).once('value', function(snapshot) {
            snapshot.forEach(function(subSnapshot) {
                bExist = true;
                existChatPairId = subSnapshot.val().pair_id;
            })
        });
        if(bExist === false)
        {
            await firebase.database().ref('chat_pair/' + uid).orderByChild('send_uid').equalTo(this.opponentUid).once('value', function(snapshot) {
                snapshot.forEach(function(subSnapshot) {
                    bExist = true;
                    existChatPairId = subSnapshot.val().pair_id;
                })
            });
        }

        if(bExist)
        {
            this.chatPairId = existChatPairId;
            return;
        }

        // If not exist create a new one
        var newChatPairRef = await firebase.database().ref('chat_pair/' +  uid).push();
        var newChatPairInfo = {
            timestamp : firebase.database.ServerValue.TIMESTAMP,
            flag: 0,
            pair_id: newChatPairRef.key,
            recv_uid : this.opponentUid,
            send_uid : this.props.user.uid,
        };
        await newChatPairRef.set(newChatPairInfo);

        newChatPairRef = firebase.database().ref('chat_pair/' +  this.opponentUid + '/' + newChatPairRef.key);
        newChatPairInfo = {
            timestamp : firebase.database.ServerValue.TIMESTAMP,
            flag: 1,
            pair_id: newChatPairRef.key,
            send_uid : this.opponentUid,
            recv_uid : this.props.user.uid,
        };
        await newChatPairRef.set(newChatPairInfo);

        // And return newly created one
        this.chatPairId = newChatPairRef.key;
    }

    fetchPairUsersInfo = (pairId) => {
        const { user } = this.props;
        const uid = user.uid;
        var userInfoList = {};

        return firebase.database().ref('chat_pair/' + uid + '/' + pairId).once('value').then((snapshot) => {
            const userInfoPromises =[]; // array to store promises
            const pairInfo = snapshot.val();
      
            var promiseVal;
            promiseVal = firebase.database().ref('users/' + pairInfo.send_uid).once('value').then((userSnapshot) => {
                var userInfo = userSnapshot.val();
                userInfoList[pairInfo.send_uid] = userInfo;
            });
            userInfoPromises.push(promiseVal);
      
            promiseVal = firebase.database().ref('users/' + pairInfo.recv_uid).once('value').then((userSnapshot) => {
                var userInfo = userSnapshot.val();
                userInfoList[pairInfo.recv_uid] = userInfo;
            });
            userInfoPromises.push(promiseVal);
      
            return Promise.all(userInfoPromises);
        })
        .then(result => {
            this.userInfoList = userInfoList;
            return userInfoList;
        })
    }

    fetchPairMessageList = async (pairId, updateMessageListCallback, appendMessageToFirstCallback) => {
        const { user } = this.props;
        const myUid = user.uid;   
        this.userInfoList = await this.fetchPairUsersInfo(pairId);
        var setMessageReceivedStatus = this.setMessageReceivedStatus;
        var changeInMessage = this.changeInMessage;
        var deleteForAllMessage = this.deleteForAllMessage;
        const userInfoList = this.userInfoList;

        var lastTimeStamp = 0;
        await firebase.database().ref('pair_message/' + pairId).orderByChild('timestamp').limitToLast(MORE_MESSAGE_COUNT).once('value', function(snapshot) {
            var pairMessageList = [];
            var messageCount = 0;
            snapshot.forEach(function(subSnapshot) {
                var messageInfo = subSnapshot.val();
                lastTimeStamp = messageInfo.timestamp;

                // check if this message is hide conversationed message
                var isHide = messageInfo[`${myUid}`] ? messageInfo[`${myUid}`] : false;
                if(isHide === true) return;

                messageCount++;

                var type = messageInfo.type ? messageInfo.type : 'text';
                var giftMessage = {
                    _id: Math.round(Math.random() * 1000000),
                    createdAt: new Date(messageInfo.timestamp),
                    user: {
                        _id: messageInfo.send_uid,
                        name: userInfoList[messageInfo.send_uid].name,
                        avatar: userInfoList[messageInfo.send_uid].photo_url,
                    },
                    key: subSnapshot.key,
                    hide: messageInfo.hide ? messageInfo.hide : false,
                    sent: messageInfo.sent ? messageInfo.sent : false,
                    received: messageInfo.received ? messageInfo.received : false,
                };
                // type : text, image
                giftMessage[type] = messageInfo.message;
                pairMessageList.unshift(giftMessage);

                // if recv_uid is mine, set message status to received
                const recv_uid = messageInfo.recv_uid;
                setMessageReceivedStatus(pairId, subSnapshot.key, recv_uid);
            });
            updateMessageListCallback(pairMessageList, messageCount);
        });
    
        firebase.database().ref('pair_message/' + pairId).orderByChild('timestamp').startAt(lastTimeStamp + 1).on('child_added', function(snapshot) {
            var messageInfo = snapshot.val();
            var type = messageInfo.type ? messageInfo.type : 'text';
            var giftMessage = {
                _id: Math.round(Math.random() * 1000000),
                createdAt: new Date(messageInfo.timestamp),
                user: {
                    _id: messageInfo.send_uid,
                    name: userInfoList[messageInfo.send_uid].name,
                    avatar: userInfoList[messageInfo.send_uid].photo_url,
                },
                key: snapshot.key,
                sent: true,
            };

            // if recv_uid is mine, set message status to received
            const recv_uid = messageInfo.recv_uid;
            setMessageReceivedStatus(pairId, snapshot.key, recv_uid);

            // type : text, image, video
            giftMessage[type] = messageInfo.message;
            appendMessageToFirstCallback(giftMessage);
        });

        firebase.database().ref('pair_message/' + pairId).orderByChild('timestamp').limitToLast(MORE_MESSAGE_COUNT).on('child_changed', function(data) {
            changeInMessage(data.key, data.val());
        });

        firebase.database().ref('pair_message/' + pairId).orderByChild('timestamp').startAt(lastTimeStamp + 1).on('child_changed', function(data) {
            changeInMessage(data.key, data.val());
        });

        firebase.database().ref('pair_message/' + pairId).on('child_removed', function(data) {
            deleteForAllMessage(data.key, data.val());
        });
    }

    setMessageReceivedStatus = (pairId, key, recv_uid) => {
        const { user } = this.props;
        if(user.uid === recv_uid)
            firebase.database().ref('pair_message/' + pairId + '/' + key).update({received:true});
    }

    deleteForAllMessage = (key, data) => {
        var { messages } = this.state;
        var foundIndex = messages.findIndex(message => message.key === key);
        if(foundIndex === -1) return;
        messages.splice(foundIndex,1);
        this.setState({messages : messages.slice()})
    }

    changeInMessage = (key, data) => {
        const { user } = this.props;
        var { messages } = this.state;
        var foundIndex = messages.findIndex(message => message.key === key);
        if(foundIndex === -1) return;

        // change sent, received status
        var message = Object.assign({}, messages[foundIndex]);
        message['sent'] = data['sent'] ? data['sent'] : false;
        message['received'] = data['received'] ? data['received'] : false;
        message['hide'] = data['hide'] ? data['hide'] : false;
        messages[foundIndex] = message;
        
        this.setState({messages : messages.slice()})
    }

  
    appendMessageToFirstCallback = (message) => {
        var newMessages = [message];
        
        if (this._isMounted === true) {
            this.setState({messages : newMessages.concat(this.state.messages)})
        }
    }

    appendMessagesToLastCallback = (newMessages, count) => {
        // Update the displayed message's begin for view more message history
        if(newMessages.length > 0)
            this.beginTimestamp = new Date(newMessages[newMessages.length-1].createdAt).getTime();

        if(count < MORE_MESSAGE_COUNT)
            this.removeLoadEarilerButton();
            
        if (this._isMounted === true) {
            this.setState({messages : this.state.messages.concat(newMessages)})
        }
    }

    updateMessageListCallback = (messages, count) => {
        // Get the displayed message's begin for view more message history
        if(messages.length > 0)
            this.beginTimestamp = new Date(messages[messages.length-1].createdAt).getTime();
        
        if(count < MORE_MESSAGE_COUNT)
            this.removeLoadEarilerButton();

        if (this._isMounted === true) {
            this.setState({messages})
        }
    }

    removeLoadEarilerButton = () => {
        this.setState({loadEarlier:false});
    }

    onLoadEarlier(appendMessagesToLastCallback) {
        const { user } = this.props;
        const myUid = user.uid;   
        var changeInMessage = this.changeInMessage;
        var setMessageReceivedStatus = this.setMessageReceivedStatus;
        const userInfoList = this.userInfoList;
        const pairId = this.chatPairId;
        firebase.database().ref('pair_message/' + pairId).orderByChild('timestamp').endAt(this.beginTimestamp - 1).limitToLast(MORE_MESSAGE_COUNT).once('value', function(snapshot) {
            var pairMessageList = [];
            var messageCount = 0;
            snapshot.forEach(function(subSnapshot) {
                messageCount++;
                var messageInfo = subSnapshot.val();
                
                var type = messageInfo.type ? messageInfo.type : 'text';
                var giftMessage ={
                    _id: Math.round(Math.random() * 1000000),
                    createdAt: new Date(messageInfo.timestamp),
                    user: {
                        _id: messageInfo.send_uid,
                        name: userInfoList[messageInfo.send_uid].name,
                        avatar: userInfoList[messageInfo.send_uid].photo_url,
                    },
                    key: subSnapshot.key,
                    hide: messageInfo.hide ? messageInfo.hide : false,
                    sent: messageInfo.sent ? messageInfo.sent : false,
                    received: messageInfo.received ? messageInfo.received : false,
                };
                // type : text, image
                giftMessage[type] = messageInfo.message;
                pairMessageList.unshift(giftMessage);

                // if recv_uid is mine, set message status to received
                const recv_uid = messageInfo.recv_uid;
                setMessageReceivedStatus(pairId, subSnapshot.key, recv_uid);    
            });
            appendMessagesToLastCallback(pairMessageList, messageCount);
        });

        firebase.database().ref('pair_message/' + pairId).orderByChild('timestamp').endAt(this.beginTimestamp - 1).limitToLast(MORE_MESSAGE_COUNT).on('child_changed', function(data) {
            changeInMessage(data.key, data.val());
        });
    }

    insertPairMessageRecord = (newMessageInfo, chatPairId) => {
        // insert new message to pair_message table and return server timestamp value
        var newMessageRef = firebase.database().ref('pair_message/' + chatPairId).push();
        return new Promise((resolve, reject) => {
            newMessageRef.set(newMessageInfo)
            .then(() => {
                return firebase.database().ref('pair_message/' + chatPairId + '/' + newMessageRef.key + '/timestamp').once('value')
                .then((snapshot) => {
                    serverTimestamp = snapshot.val();
                    resolve(serverTimestamp);
                })
                .catch(() => {
                    reject(0);
                });
            })
            .catch(() => {
                reject(0);
            })
        })
    }

    sendPairMessage = async (newMessageInfo, chatPairId, myUID, opponentUid, type, message) => {
        const timestamp = await this.insertPairMessageRecord(newMessageInfo, chatPairId);
        
        if(timestamp == 0) return;
        
        // update my pair info with the same timestamp
        var myPairInfo = {
            timestamp : timestamp,
            flag : 0,
            last_message : message,
            type: type,
            pair_id : chatPairId,
            recv_uid : opponentUid,
            send_uid : myUID,
        };
        firebase.database().ref('chat_pair/' + myUID + '/' + chatPairId).set(myPairInfo)
        .then(function() {})
        .catch(function(error) {
            console.log("Error saving pair info into chat_pair table: ", error);
        });
        
        // update opponent pair info
        var opponentPairInfo = {
            timestamp : timestamp,
            flag : 1,
            last_message : message,
            type: type,
            pair_id : chatPairId,
            recv_uid : opponentUid,
            send_uid : myUID,
        };
        firebase.database().ref('chat_pair/' + opponentUid + '/' + chatPairId).set(opponentPairInfo)
        .then(function() {})
        .catch(function(error) {
            console.log("Error saving pair info into chat_pair table: ", error);
        });
    }

    onSendText(messages = []) {
        if (messages.length > 0) {
            var newMessageInfo = {
                timestamp : firebase.database.ServerValue.TIMESTAMP,
                message : messages[0].text,
                type: "text",
                recv_uid : this.opponentUid,
                send_uid : this.props.user.uid,
                sent: true,
            };
            
            var myUID = this.props.user.uid;
            var opponentUid = this.opponentUid;
            var chatPairId = this.chatPairId;
            var message = messages[0].text;
            
            this.sendPairMessage(newMessageInfo, chatPairId, myUID, opponentUid, "text", message);

            sendNotificationToUser(opponentUid, {
                "notification": {
                    "title": "New Message from " + (this.userInfoList[myUID].name ? this.userInfoList[myUID].name : 'someone'),
                    "body": message,
                    "sound": "default",
                    "click_action" : ".MainActivity"
                },
                "priority": "high"
            });
        }
    }
    
    renderBubble(props) {
        if (isSameUser(props.currentMessage, props.previousMessage) && isSameDay(props.currentMessage, props.previousMessage)) {
            return (
            <Bubble
                {...props}
                textStyle={{
                    left: {
                        color: Colors.BLACK_COLOR,
                    },
                    right: {
                        color: Colors.BLACK_COLOR,
                    }
                }}
                wrapperStyle={{
                    left: {
                        marginLeft: 8,
                        backgroundColor: '#e2edf9',
                    },
                    right: {
                        backgroundColor: Colors.PRIMARY_COLOR,
                    }
                }}
                tickStyle = {{
                    color: Colors.BLACK_COLOR,
                }}
            />
            );
        }
        return (
            <View>
                {props.position === 'left' && <Text style={{textAlign:props.position}}>{props.currentMessage.user.name}</Text>}
                <Bubble
                    {...props}
                    textStyle={{
                        left: {
                            color: Colors.BLACK_COLOR,
                        },
                        right: {
                            color: Colors.BLACK_COLOR,
                        }
                    }}
                    wrapperStyle={{
                        left: {
                            backgroundColor: '#e2edf9',
                        },
                        right: {
                            backgroundColor: Colors.PRIMARY_COLOR,
                        }
                    }}
                    tickStyle = {{
                        color: Colors.BLACK_COLOR,
                    }}
                />
            </View>
        );
    }

    renderTime(props) {
        return (
            <Time
                {...props}
                textStyle={{
                    left: {
                        color: Colors.BLACK_COLOR,
                    },
                    right: {
                        color: Colors.BLACK_COLOR,
                    }
                }}
            />
        );
    }
    
    renderMessage = (messageProps) => {
        const myUid =  messageProps.user._id;
        const currentMessage = messageProps.currentMessage;
        const messageSenderUid = currentMessage.user._id;
        const isMyMessage = (myUid === messageSenderUid);
        const isHide = currentMessage['hide'] ? currentMessage['hide'] : false;
        if(isMyMessage && isHide) return;

        return <Message { ...messageProps}/>;
    }

    renderMessageImage(props){
        return <MessageImage {...props} />
    }

    renderDay(props) {
        return <Day {...props} textStyle={{color: 'black', fontSize:20}} 
            containerStyle={{flex:1, justifyContent: 'flex-start',alignItems: 'flex-start', margin: 10, borderBottomColor:'#aaa', borderBottomWidth:1}}
            wrapperStyle={{}}/>
    }

    renderAvatar(props) {
        return <Avatar {...props} imageStyle={{
            left: {
                height: 36,
                width: 36,
                borderRadius: 18,
            },
        }}/>;
    }
    
    onPressAvatar(props){
        console.log("Avator Pressed.")
    }

    onSendPhoto = () => {
        this.setState({photoSelectDialogShow:true});
    }

    _onSendPhoto = (url) => {    
        var newMessageInfo = {
            timestamp : firebase.database.ServerValue.TIMESTAMP,
            message : url,
            type: "image",
            recv_uid : this.opponentUid,
            send_uid : this.props.user.uid,
            sent: true,
        };
        
        var myUID = this.props.user.uid;
        var opponentUid = this.opponentUid;
        var chatPairId = this.chatPairId;
        var message = url;
        
        this.sendPairMessage(newMessageInfo, chatPairId, myUID, opponentUid, "image", message);

        sendNotificationToUser(opponentUid, {
            "notification": {
                "title": "New Message from " + (this.userInfoList[myUID].name ? this.userInfoList[myUID].name : 'someone'),
                "body": "[Photo message]",
                "sound": "default",
                "click_action" : ".MainActivity"
            },
            "priority": "high"
        });
    }

    pickPhotoFromCamera() {
        const { user } = this.props;
        ImagePicker.openCamera({
            mediaType: "photo",
        }).then(async(image) => {
            var photo = await UPLOAD_CHAT_PHOTO(image.path, user.uid, 'chat', 'image/jpg');
            this._onSendPhoto(photo);
        }).catch(e => {
            console.log(e);
            Alert.alert(e.message ? e.message : e);
        });
    }

    pickPhotoFromGallery() {
        const { user } = this.props;
        ImagePicker.openPicker({
            mediaType: "photo",
        }).then(async(image) => {
            var photo = await UPLOAD_CHAT_PHOTO(image.path, user.uid, 'chat', 'image/jpg');
            this._onSendPhoto(photo);
        }).catch(e => {
            console.log(e);
            Alert.alert(e.message ? e.message : e);
        });
    }

    onSendVoice = () => {

    }

    renderCustomActions(props) {
        return (
            <View style={{flexDirection:'row', alignSelf:'center'}}>
                <TouchableOpacity style={{marginLeft:10, justifyContent:'center', alignItems:'center', width:30, height:30, borderRadius:15, backgroundColor:Colors.PRIMARY_COLOR}} onPress={() => this.onSendPhoto()}>
                    <FontAwesomeIcon name="paperclip" size={20} color={Colors.WHITE_COLOR} />
                </TouchableOpacity>
                {/* <TouchableOpacity style={{marginLeft:10, justifyContent:'center', alignItems:'center', width:30, height:30, borderRadius:15, backgroundColor:Colors.PRIMARY_COLOR}} onPress={() => this.onSendVoice()}>
                    <FontAwesomeIcon name="microphone" size={20} color={Colors.WHITE_COLOR} />
                </TouchableOpacity> */}
            </View>
        )
    }

    onLongPress(context, currentMessage) {
        this.showActionSheet(currentMessage);
    }
    
    handleImageLongPress = (currentMessage) => {
        this.showActionSheet(currentMessage);
    }

    showActionSheet = (currentMessage) => {
        const { messages } = this.state;
        const { user } = this.props;
        const messageCountOnScreen = messages.length;
        const currentMessageIndex = messages.findIndex(message => message.key == currentMessage.key);
        const isLastMessage = (currentMessageIndex === 0);  // is used to update opponent last message info
        var isLastMyShowMessage = true;       // is used to update my last message info
        for(var i=0;i<currentMessageIndex;i++){
            const isMyMessage = (messages[i].user._id === user.uid);
            const isHideMessage = messages[i]['hide'] ? messages[i]['hide'] : false;
            if(!isMyMessage) {
                isLastMyShowMessage = false;
                break;
            }
            if(!isHideMessage) {
                isLastMyShowMessage = false;
                break;
            }
        }
        var senderInfo = currentMessage.user;
        if(senderInfo === null || senderInfo === undefined)
            return;

        var sender_id = senderInfo._id;
        let isMyAction = false;
        if(sender_id == user.uid) isMyAction = true;

        var messageType;
        if(currentMessage.hasOwnProperty('image')) {
            messageType = 'image'
        }
        else if(currentMessage.hasOwnProperty('text')) {
            messageType = 'text'
        }
           
        var buttons = [
            'Forward',
        ];

        if(messageType === "text")
            buttons.push('Copy')
        else
            buttons.push('Download')

        if(isMyAction){
            buttons.push('Delete for Self');
            buttons.push('Delete for All');
        }
        buttons.push('Cancel')
           
        var DESTRUCTIVE_INDEX = isMyAction ? 4 : 2;
        var CANCEL_INDEX = isMyAction ? 5 : 3;
           
        ActionSheet.showActionSheetWithOptions({
            options: buttons,
            cancelButtonIndex: CANCEL_INDEX,
            destructiveButtonIndex: DESTRUCTIVE_INDEX,
            tintColor: 'blue'
        },
        (buttonIndex) => {
            if(buttonIndex === 0){  //forward
                this.props.navigation.navigate('ForwardMessage', {
                    message: currentMessage,
                })
            }
            else if(buttonIndex === 1){
                if(messageType === 'text') {    // copy
                    Clipboard.setString(currentMessage.text);
                }
                else if(messageType === 'image') {  //download
                    DOWNLOAD_PHOTO_FILE(currentMessage.image);
                }
            }
            else if(isMyAction && buttonIndex === 2){   // delete for self
                if(isLastMyShowMessage) 
                    this.updateMyLastMessageInfo(currentMessage.key);
                firebase.database().ref('pair_message/' + this.chatPairId + '/' + currentMessage.key).update({
                    hide: true,
                });
            }
            else if(isMyAction && buttonIndex === 3){   // delete for all
                if(isLastMessage) 
                    this.updateOpponentLastMessageInfo(currentMessage.key);
                if(isLastMyShowMessage) 
                    this.updateMyLastMessageInfo(currentMessage.key);
                firebase.database().ref('pair_message/' + this.chatPairId + '/' + currentMessage.key).remove();
            }
        });
    }

    updateOpponentLastMessageInfo = (deletingMessageKey) => {
        const { messages } = this.state;
        const { user } = this.props;
        const opponentUid = this.opponentUid;
        const chatPairId = this.chatPairId;
        const myUid = user.uid;
        var foundLastMessage = false;
        this.haveToUpdateOpponentLastMessageInfo = true;

        for(var i=0;i<messages.length;i++){
            const messageInfo = messages[i];
            if(messageInfo.key == deletingMessageKey) // will be soon deleted, so skip this
                continue;
            const isOpponentMessage = (messageInfo.user._id === opponentUid);
            const isHide = messageInfo['hide'] ? messageInfo['hide'] : false;
            if(!isOpponentMessage || (isOpponentMessage && !isHide)){ //set this message as a last message of opponent
                foundLastMessage = true;
                const flag = (isOpponentMessage ? 1 : 0);
                var type, message;
                if(messageInfo.hasOwnProperty('image')) {
                    type = 'image';
                    message = messageInfo.image;
                }
                else if(messageInfo.hasOwnProperty('text')) {
                    type = 'text'
                    message = messageInfo.text;
                }
                var opponentPairInfo = {
                    timestamp : new Date(messageInfo.createdAt).getTime(),
                    flag : flag,
                    last_message : message,
                    type: type,
                    pair_id : chatPairId,
                    recv_uid : (flag ? myUid : opponentUid),
                    send_uid : (flag ? opponentUid : myUid),
                };
                firebase.database().ref('chat_pair/' + opponentUid + '/' + chatPairId).set(opponentPairInfo)
                .then(function() {
                })
                .catch(function(error) {
                    console.log("Error saving pair info into chat_pair table: ", error);
                });
                this.haveToUpdateOpponentLastMessageInfo = false;
                break;
            }
        }
        if (!foundLastMessage) {// if don't found opponenet last message set empty string to hide it in list
            firebase.database().ref('chat_pair/' + opponentUid + '/' + chatPairId).update({last_message: ''})
        }
    }

    updateMyLastMessageInfo = (deletingMessageKey) => {
        const { messages } = this.state;
        const { user } = this.props;
        const opponentUid = this.opponentUid;
        const chatPairId = this.chatPairId;
        const myUid = user.uid;
        var foundLastMessage = false;
        this.haveToUpdateMyLastMessageInfo = true;

        for(var i=0;i<messages.length;i++){
            const messageInfo = messages[i];
            if(messageInfo.key == deletingMessageKey) // will be soon deleted, so skip this
                continue;
            const isMyMessage = (messageInfo.user._id === myUid);
            const isHide = messageInfo['hide'] ? messageInfo['hide'] : false;
            if(!isMyMessage || (isMyMessage && !isHide)){ //set this message as a last message of opponent
                foundLastMessage = true;
                const flag = (isMyMessage ? 0 : 1);
                var type, message;
                if(messageInfo.hasOwnProperty('image')) {
                    type = 'image';
                    message = messageInfo.image;
                }
                else if(messageInfo.hasOwnProperty('text')) {
                    type = 'text'
                    message = messageInfo.text;
                }
                var myPairInfo = {
                    timestamp : new Date(messageInfo.createdAt).getTime(),
                    flag : flag,
                    last_message : message,
                    type: type,
                    pair_id : chatPairId,
                    recv_uid : (flag ? myUid : opponentUid),
                    send_uid : (flag ? opponentUid : myUid),
                };
                firebase.database().ref('chat_pair/' + myUid + '/' + chatPairId).set(myPairInfo)
                .then(function() {
                })
                .catch(function(error) {
                    console.log("Error saving pair info into chat_pair table: ", error);
                });
                this.haveToUpdateMyLastMessageInfo = false;
                break;
            }
        }
        if (!foundLastMessage) {// if don't found my last message set empty string to hide it in list
            firebase.database().ref('chat_pair/' + myUid + '/' + chatPairId).update({last_message: ''})
        }
    }

	render() {
        const { user } = this.props;
        const { messages, loadEarlier } = this.state;
		return (
			<View style={styles.mainContainer}>
				<View>
					<NavigationHeader haveBack={true} title="Messages"/>
				</View>
				<View style={{flex:1}}>
                    <GiftedChat
                        messages={messages}
                        onSend={this.onSendText}
                        onLongPress={this.onLongPress}
                        loadEarlier={loadEarlier}
                        onLoadEarlier={() => this.onLoadEarlier(this.appendMessagesToLastCallback)}
                        isLoadingEarlier={this.state.isLoadingEarlier}
                        onPressAvatar ={this.onPressAvatar.bind(this)}
                        renderAvatarOnTop={true}
                        renderActions={this.renderCustomActions}
                        renderMessage={this.renderMessage}
                        renderMessageImage={this.renderMessageImage}
                        lightboxProps={{
                            onLongPress : (currentMessage) => this.handleImageLongPress(currentMessage)
                        }}
                        placeholder="Write your message..."

                        user={{
                            _id: user.uid,
                        }}

                        textInputProps={
                            style = {
                                borderWidth:1, 
                                borderRadius:18, 
                                fontSize:10, 

                                paddingHorizontal: 15,
                                paddingVertical: 0,
                                fontSize: 16,
                                lineHeight: 20,
                                marginTop: Platform.select({
                                    ios: 6,
                                    android: 0,
                                }),
                                marginBottom: Platform.select({
                                    ios: 5,
                                    android: 3,
                                }),
                                marginRight: 10,
                            }
                        }

                        renderBubble={this.renderBubble}
                        renderTime={this.renderTime}
                        renderDay={this.renderDay}
                        renderAvatar={this.renderAvatar.bind(this)}
                    />
                    <Dialog
                        onDismiss={() => this.setState({ photoSelectDialogShow: false })}
                        onTouchOutside={() => this.setState({ photoSelectDialogShow: false })}
                        onHardwareBackPress={() => this.setState({ photoSelectDialogShow: false })}
                        rounded={false}
                        width={0.9}
                        visible={this.state.photoSelectDialogShow}
                        dialogAnimation={new SlideAnimation({
                            slideFrom: 'bottom',
                        })}>
                        <DialogContent
                            style={{backgroundColor: 'white',}}>
                            <View style={{flexDirection:'column', alignItems: 'center', marginTop:20,}}>              
                                <Text style={{fontSize:18, color: Colors.BLACK_COLOR, marginBottom: 10,}}>Select Photo from</Text>
                                <View style={styles.dialogButtonGroupStyle}>
                                    <TouchableOpacity style={styles.dialogButtonStyle}
                                    onPress={() => {this.setState({ photoSelectDialogShow: false }); this.pickPhotoFromGallery();}}>
                                        <Text style={{fontSize:16,color:'black',textAlign:'center',}}>From Gallery</Text>
                                    </TouchableOpacity>
                                    <TouchableOpacity style={styles.dialogButtonStyle}
                                    onPress={() => {this.setState({ photoSelectDialogShow: false }); this.pickPhotoFromCamera();}}>
                                        <Text style={{fontSize:16,color:'black',textAlign:'center',}}>From Camera</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </DialogContent>
                    </Dialog>
				</View>
			</View>
		);
	}
}

const mapStateToProps = state => {
	return {
		user : state.UserReducer.user,
	};
};
  
const mapDispatchToProps = {
	fetchDBUserList,
};
  
export default connect(mapStateToProps, mapDispatchToProps)(ChatRoom);

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
	},
		
    avatar: {
        width: 70,
		height: 70,
		marginRight: 15,
        borderRadius: 35,
    },

	seperator: {
		marginVertical: 1, 
		height: 2, 
		backgroundColor: Colors.LIGHT_GREY,
	},

    dialogButtonGroupStyle: {
		flexDirection: 'row',
		marginTop: 10,
		height: 50,
		alignItems: 'center',
    },
  
    dialogButtonStyle: {
		height: 50, 
		width: '45%',
		justifyContent: 'center', 
		alignItems: 'center',
		backgroundColor: 'white', 
		borderColor: 'gray', 
		borderWidth: 1, 
		marginRight: 10,
    },
});
