import React from 'react';
import {
	StyleSheet,
	Text,
	View,
	Alert,
	FlatList,
	Dimensions,
	TouchableOpacity,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import Contacts from 'react-native-contacts';
import Share from 'react-native-share';
import { parsePhoneNumberFromString } from 'libphonenumber-js';
import Permissions from 'react-native-permissions';

import NavigationHeader from '../../Components/NavigationHeader'
import Colors from '../../Constants/Colors';

import { connect } from 'react-redux';
import { fetchDBUserList } from '../../Actions/UserActions';

const { width, height } = Dimensions.get('window');

class ChatCreate extends React.Component {
	state = {
		contactPhoneNumberList: [],
	};

	constructor(props) {
		super();
	}

	componentDidMount() {
		var updateContactPhoneNumberList = this.updateContactPhoneNumberList;

		Permissions.request('contacts').then(response => {
			if(response === 'authorized') {
				const { user } = this.props;
				const myPhoneNumberObj = parsePhoneNumberFromString(user.phoneNumber);
				const myCallingCode = myPhoneNumberObj.countryCallingCode;
				Contacts.getAll((err, contacts) => {
					if (err === 'denied'){
						// error
					} else {
						var contactPhoneNumberList = []; 
						for(var i=0;i<contacts.length;i++){
							const phoneNumbers = contacts[i].phoneNumbers;
							for(var j=0;j<phoneNumbers.length;j++){
								var phoneNumberOrig = phoneNumbers[j].number;
								const yourPhoneNumberObj = parsePhoneNumberFromString(phoneNumberOrig);
								if (yourPhoneNumberObj === undefined) // has no country calling code
								{
									if(phoneNumberOrig.charAt(0) === '0')
										phoneNumberOrig = phoneNumberOrig.slice(1);
									phoneNumberOrig = "+" + myCallingCode + phoneNumberOrig;
								}
								var phoneNumberSanitized = phoneNumberOrig.replace(/\D/g,'');
								contactPhoneNumberList.push(phoneNumberSanitized);
							}
						}
						updateContactPhoneNumberList(contactPhoneNumberList);
					}
				});
			}
		})

		new Promise((resolve, reject) => {
			this.props.fetchDBUserList(
				resolve,
				reject,
			)
		})
		.then( success => {
			console.log("Successfully Fetched User List!");
		})
		.catch( error => {
			console.log(error)
		})
	}

	updateContactPhoneNumberList = (contactPhoneNumberList) => {
		this.setState({contactPhoneNumberList});
	}

	onStartMessage = (opponentUid) => {
		this.props.navigation.navigate('ChatRoom', {
			opponentUid: opponentUid,
		});
	}

	_RenderItem = (item, index) => {
		if(item.uid === this.props.user.uid) return;
		return (
			<TouchableOpacity style={styles.chatContainer} onPress={() => this.onStartMessage(item.uid)}>
				<FastImage
                    style={styles.avatar}
                    source={{
                        uri: item.photo_url,
                        priority: FastImage.priority.high,
                    }}
                    resizeMode={FastImage.resizeMode.contain}
                />
				<View style={styles.chatSubContainer}>
					<Text style={styles.nameText}>{item.name}</Text>
					<Text style={styles.messageText} numberOfLines={2}>{item.address}</Text>
				</View>
			</TouchableOpacity>
		)
	}

	getMyUserList = (userList, contactPhoneNumberList) => {
		var myUserList = [];
		var contactPhoneNumbers = contactPhoneNumberList.join('-');
		for(var i=0;i<userList.length;i++){
			var dbPhoneNumber = userList[i].phone_number ? userList[i].phone_number : null;
			if(dbPhoneNumber === null) continue;
			var dbPhoneNumberSantized = dbPhoneNumber.replace(/\D/g,'');
			var foundIndex = contactPhoneNumbers.search(dbPhoneNumberSantized);
			if(foundIndex >= 0)
				myUserList.push(userList[i])
		}
		return myUserList;
	}

	handleShareClick = () => {
        let shareOptions = {
            title: "Send Invitation",
            message: "Check out KChat App for your smart phone.",
            url: "http://facebook.github.io/react-native/",
            // subject: "Check out KChat App for your smart phone. http://facebook.github.io/react-native/"
        };
        Share.open(shareOptions)
            .then((res) => { console.log(res) })
            .catch((err) => { err && console.log(err); });
    }

	render() {
		const { contactPhoneNumberList } = this.state;
		const { userList } = this.props;
		const myUserList = this.getMyUserList(userList, contactPhoneNumberList);

		return (
			<View style={styles.mainContainer}>
				<View>
					<NavigationHeader haveBack={true} title="Create a Chat"/>
				</View>
				<TouchableOpacity onPress={()=> this.handleShareClick()}>
					<View style={{marginTop: 20, marginHorizontal: 20, backgroundColor: Colors.PRIMARY_COLOR, borderRadius: 20}}>
						<Text style={{textAlign:'center', margin: 10, fontSize: 16}}>Send Invitation</Text>
					</View>
				</TouchableOpacity>
				{/* <TouchableOpacity onPress={()=> this.props.navigation.navigate('NewGroupChat')}>
					<View style={{marginTop: 20, marginHorizontal: 20, backgroundColor: Colors.PRIMARY_COLOR, borderRadius: 20}}>
						<Text style={{textAlign:'center', margin: 10, fontSize: 16}}>New Group Chat</Text>
					</View>
				</TouchableOpacity> */}
				<View style={{flex:1, alignItems:'center'}}>
					<Text style={styles.descriptionText}>Please Select A Person You Want to Chat With.</Text>
					<FlatList
						data={myUserList}
						keyExtractor={(item, index) => item.uid}
						renderItem={({item, index}) => this._RenderItem(item, index)}
					/>
				</View>
			</View>
		);
	}
}

const mapStateToProps = state => {
	return {
		user : state.UserReducer.user,
		userList : state.UserReducer.userList,
		isFetching: state.UserReducer.isFetching,
		isSuccess: state.UserReducer.isSuccess,
	};
};
  
const mapDispatchToProps = {
	fetchDBUserList,
};
  
export default connect(mapStateToProps, mapDispatchToProps)(ChatCreate);

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
	},

    loadingContainer: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: '#fff',
	},

	chatContainer: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		marginHorizontal: 10, 
		marginVertical: 15, 
	},

	chatSubContainer: {
		width: width - 100,
		flexDirection: 'column',
		alignItems: 'flex-start',
		marginLeft: 20,
	},

	avatar: {
		width: 70,
		height: 70,
		borderRadius: 35,
	},
	
	nameText: {
		fontSize: 18, 
		color: Colors.BLACK_COLOR,
	},
	
	messageText: {
		fontSize: 14, 
		color: Colors.GREY,
	},
	
	descriptionText: {
		textAlign: 'center', 
		width: '70%', 
		fontSize: 16, 
		marginTop: 20,
		color: Colors.BLACK_COLOR,
	},

});
