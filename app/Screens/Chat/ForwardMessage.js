import React from 'react';
import {
	StyleSheet,
	Text,
	View,
	Alert,
	FlatList,
	Dimensions,
	ActivityIndicator,
	TouchableOpacity,
} from 'react-native';
import Contacts from 'react-native-contacts';
import FastImage from 'react-native-fast-image';
import firebase from 'react-native-firebase';
import { parsePhoneNumberFromString } from 'libphonenumber-js';
import Permissions from 'react-native-permissions';

import NavigationHeader from '../../Components/NavigationHeader';
import Colors from '../../Constants/Colors';

import { connect } from 'react-redux';
import { fetchDBUserList } from '../../Actions/UserActions';

const { width, height } = Dimensions.get('window');

class ForwardMessage extends React.Component {
	state = {
		loading: false,
		contactPhoneNumberList: [],
		refresh: false,
	};

	constructor(props) {
		super();

		this.message  = props.navigation.getParam('message', null);
	}

	componentDidMount() {
		var updateContactPhoneNumberList = this.updateContactPhoneNumberList;

		Permissions.request('contacts').then(response => {
			if(response === 'authorized') {
				const { user } = this.props;
				const myPhoneNumberObj = parsePhoneNumberFromString(user.phoneNumber);
				const myCallingCode = myPhoneNumberObj.countryCallingCode;
				Contacts.getAll((err, contacts) => {
					if (err === 'denied'){
						// error
					} else {
						var contactPhoneNumberList = []; 
						for(var i=0;i<contacts.length;i++){
							const phoneNumbers = contacts[i].phoneNumbers;
							for(var j=0;j<phoneNumbers.length;j++){
								var phoneNumberOrig = phoneNumbers[j].number;
								const yourPhoneNumberObj = parsePhoneNumberFromString(phoneNumberOrig);
								if (yourPhoneNumberObj === undefined) // has no country calling code
								{
									if(phoneNumberOrig.charAt(0) === '0')
										phoneNumberOrig = phoneNumberOrig.slice(1);
									phoneNumberOrig = "+" + myCallingCode + phoneNumberOrig;
								}
								var phoneNumberSanitized = phoneNumberOrig.replace(/\D/g,'');
								contactPhoneNumberList.push(phoneNumberSanitized);
							}
						}
						updateContactPhoneNumberList(contactPhoneNumberList);
					}
				})
			}
		})

		new Promise((resolve, reject) => {
			this.props.fetchDBUserList(
				resolve,
				reject,
			)
		})
		.then( success => {
			console.log("Successfully Fetched User List!");
		})
		.catch( error => {
			console.log(error)
		})
	}

	updateContactPhoneNumberList = (contactPhoneNumberList) => {
		this.setState({contactPhoneNumberList});
	}

	getMyUserList = (userList, contactPhoneNumberList) => {
		var myUserList = [];
		var contactPhoneNumbers = contactPhoneNumberList.join('-');
		for(var i=0;i<userList.length;i++){
			var dbPhoneNumber = userList[i].phone_number ? userList[i].phone_number : null;
			if(dbPhoneNumber === null) continue;
			var dbPhoneNumberSantized = dbPhoneNumber.replace(/\D/g,'');
			var foundIndex = contactPhoneNumbers.search(dbPhoneNumberSantized);
			if(foundIndex >= 0)
				myUserList.push(userList[i])
		}
		return myUserList;
	}

	openNewChatPair = async(opponentUid) => {
        if(!this.props.user) return;

        const uid = this.props.user.uid;

        // Check if the chat pair already exist
        var bExist = false;
        var existChatPairId = '';
        await firebase.database().ref('chat_pair/' + uid).orderByChild('recv_uid').equalTo(opponentUid).once('value', function(snapshot) {
            snapshot.forEach(function(subSnapshot) {
                bExist = true;
                existChatPairId = subSnapshot.val().pair_id;
            })
        });
        if(bExist === false)
        {
			await firebase.database().ref('chat_pair/' + uid).orderByChild('send_uid').equalTo(opponentUid).once('value', function(snapshot) {
				snapshot.forEach(function(subSnapshot) {
					bExist = true;
					existChatPairId = subSnapshot.val().pair_id;
				})
			});
		}

        if(bExist)
        {
            return existChatPairId;
        }

        // If not exist create a new one
        var newChatPairRef = await firebase.database().ref('chat_pair/' +  uid).push();
        var newChatPairInfo = {
            ctime : firebase.database.ServerValue.TIMESTAMP,
            flag: 0,
            pair_id: newChatPairRef.key,
            recv_uid : opponentUid,
            send_uid : this.props.user.uid,
        };
        await newChatPairRef.set(newChatPairInfo);

        newChatPairRef = firebase.database().ref('chat_pair/' +  opponentUid + '/' + newChatPairRef.key);
        newChatPairInfo = {
            ctime : firebase.database.ServerValue.TIMESTAMP,
            flag: 1,
            pair_id: newChatPairRef.key,
            send_uid : this.opponentUid,
            recv_uid : this.props.user.uid,
        };
        await newChatPairRef.set(newChatPairInfo);

        // And return newly created one
        return newChatPairRef.key;
    }

	onForwardMessage = async (userInfo) => {
		const opponentUid = userInfo.uid;
		const alreadySent = (userInfo['sent'] ? userInfo['sent'] : false);
		if(alreadySent) return;
		if(this.message === null){
			Alert.alert("Error", "Can't get message, Please try again.")
		}
		
        var messageType;
        if(this.message.hasOwnProperty('image')) {
			messageType = 'image';
			message = this.message.image;
        }
        else if(this.message.hasOwnProperty('text')) {
            messageType = 'text'
			message = this.message.text;
		}
		
		var newMessageInfo = {
            timestamp : firebase.database.ServerValue.TIMESTAMP,
            message : message,
            type: messageType,
            recv_uid : opponentUid,
            send_uid : this.props.user.uid,
            sent: true,
        };
        
		var chatPairId = await this.openNewChatPair(opponentUid);
		userInfo['sent'] = true;      
        await this.sendPairMessage(newMessageInfo, chatPairId, this.props.user.uid, opponentUid, messageType, message);
		this.setState({refresh: !this.state.refresh});
	}

	insertPairMessageRecord = (newMessageInfo, chatPairId) => {
        // insert new message to pair_message table and return server timestamp value
        var newMessageRef = firebase.database().ref('pair_message/' + chatPairId).push();
        return new Promise((resolve, reject) => {
            newMessageRef.set(newMessageInfo)
            .then(() => {
                return firebase.database().ref('pair_message/' + chatPairId + '/' + newMessageRef.key + '/timestamp').once('value')
                .then((snapshot) => {
                    serverTimestamp = snapshot.val();
                    resolve(serverTimestamp);
                })
                .catch(() => {
                    reject(0);
                });
            })
            .catch(() => {
                reject(0);
            })
        })
    }

	sendPairMessage = async (newMessageInfo, chatPairId, myUID, opponentUid, type, message) => {
        const timestamp = await this.insertPairMessageRecord(newMessageInfo, chatPairId);
        
        if(timestamp == 0) return;
        
        // update my pair info with the same timestamp
        var myPairInfo = {
            timestamp : timestamp,
            flag : 0,
            last_message : message,
            type: type,
            pair_id : chatPairId,
            recv_uid : opponentUid,
            send_uid : myUID,
        };
        firebase.database().ref('chat_pair/' + myUID + '/' + chatPairId).set(myPairInfo)
        .then(function() {})
        .catch(function(error) {
            console.log("Error saving pair info into chat_pair table: ", error);
        });
        
        // update opponent pair info
        var opponentPairInfo = {
            timestamp : timestamp,
            flag : 1,
            last_message : message,
            type: type,
            pair_id : chatPairId,
            recv_uid : opponentUid,
            send_uid : myUID,
        };
        firebase.database().ref('chat_pair/' + opponentUid + '/' + chatPairId).set(opponentPairInfo)
        .then(function() {})
        .catch(function(error) {
            console.log("Error saving pair info into chat_pair table: ", error);
        });
    }

	_RenderItem = (item, index) => {
		if(item.uid === this.props.user.uid) return;
		return (
			<View style={styles.chatContainer}>
				<FastImage
                    style={styles.avatar}
                    source={{
                        uri: item.photo_url,
                        priority: FastImage.priority.high,
                    }}
                    resizeMode={FastImage.resizeMode.contain}
                />
				<View style={styles.chatSubContainer}>
					<Text style={styles.nameText}>{item.name}</Text>
					<Text style={styles.messageText} numberOfLines={2}>{item.address}</Text>
				</View>
				<TouchableOpacity style={{alignItems: 'center', marginRight: 20,}} onPress={() => this.onForwardMessage(item)}>
					{ item['sent'] ?
					<View style={styles.sentButton}>
						<Text style={styles.sentButtonText}>Sent</Text>
					</View>
					:
					<View style={styles.sendButton}>
						<Text style={styles.sendButtonText}>Send</Text>
					</View>
					}
				</TouchableOpacity>
			</View>
		)
	  }

	render() {
		const { contactPhoneNumberList, refresh } = this.state;
		const { userList } = this.props;
		const myUserList = this.getMyUserList(userList, contactPhoneNumberList);

		return (
			<View style={styles.mainContainer}>
				<View>
					<NavigationHeader haveBack={true} title="Chat Member"/>
				</View>
				<View style={{flex:1, alignItems:'center'}}>
					<Text style={styles.descriptionText}>Please Select A Person You Want to Forward A Message.</Text>
					<FlatList
						data={myUserList}
						extraData={refresh}
						keyExtractor={(item, index) => item.uid}
						renderItem={({item, index}) => this._RenderItem(item, index)}
					/>
				</View>
			</View>
		);
	}
}

const mapStateToProps = state => {
	return {
		user : state.UserReducer.user,
		userList : state.UserReducer.userList,
		isFetching: state.UserReducer.isFetching,
		isSuccess: state.UserReducer.isSuccess,
	};
};
  
const mapDispatchToProps = {
	fetchDBUserList,
};
  
export default connect(mapStateToProps, mapDispatchToProps)(ForwardMessage);

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
	},

    loadingContainer: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: '#fff',
	},

	chatContainer: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		marginHorizontal: 10, 
		marginVertical: 15, 
	},

	chatSubContainer: {
		width: width - 200,
		flexDirection: 'column',
		alignItems: 'flex-start',
		marginLeft: 20,
	},

	avatar: {
		width: 70,
		height: 70,
		borderRadius: 35,
	},
	
	nameText: {
		fontSize: 18, 
		color: Colors.BLACK_COLOR,
	},
	
	messageText: {
		fontSize: 14, 
		color: Colors.GREY,
	},
	
	descriptionText: {
		textAlign:'center', 
		marginTop: 20, 
		width: '70%', 
		fontSize: 16, 
		color: Colors.BLACK_COLOR,
	},

	sentButton: {
		backgroundColor: '#fff', 
		borderRadius: 20,
		borderWidth: 1,
		borderColor: Colors.PRIMARY_COLOR,
	},
	
	sentButtonText: {
		marginVertical: 10,
		marginHorizontal: 20,
		fontSize: 16, 
		color: Colors.PRIMARY_COLOR,
	},

	sendButton: {
		backgroundColor: Colors.PRIMARY_COLOR, 
		borderRadius: 20,
	},

	sendButtonText: {
		marginVertical: 10,
		marginHorizontal: 20,
		fontSize: 16, 
		color: '#fff',
	},
});
