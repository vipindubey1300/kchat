import React from 'react';
import {
	StyleSheet,
	Text,
	View,
	FlatList,
	Dimensions,
	ActivityIndicator,
	TouchableOpacity,
} from 'react-native';
import firebase from 'react-native-firebase';
import FastImage from 'react-native-fast-image';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import Swipeout from 'react-native-swipeout';

import Colors from '../../Constants/Colors';
import { timeAgoString } from '../../Utils/DateTimeUtil';

const { width, height } = Dimensions.get('window');

import { connect } from 'react-redux';

class ChatList extends React.Component {
	state = {
		loading: false,
		activeRow: -1,
		rowID: null,
		chatPairList: [],
		chatGroupList: [],
		refresh: false,
	};

	constructor(props) {
		super();
		
		this.chatPairListener = null;
		this.chatList = [];
		this.btnsDefault = [ {
			component: 
				<View style={{flex: 1, justifyContent:'center', alignItems:'center'}}>
					<FontAwesomeIcon name="trash"  size={30} color="white" />
					<Text style={{color:'white', fontWeight:'bold', fontSize:12, marginTop:5}}>DELETE</Text>
				</View>,
			onPress: this.handleDeleteConversation,
			backgroundColor:'red',
		} ];
	}
    
    componentWillMount() {
		const { user, navigation } = this.props;
		const uid = user.uid;
		var updateChatPairList = this.updateChatPairList;

		this.timer = setInterval(this.updateChatList, 30000);

        this.focusListener = navigation.addListener("willFocus", () => {
            this.refreshChatPairListInfo();
		});
		
		this.chatPairListener = firebase.database().ref('chat_pair/' + uid).orderByChild('timestamp').on('value', snapshot => {
			var chatPairList = [];
			snapshot.forEach(function(subSnapshot) {
				var pairInfo = subSnapshot.val();
				var puser = (pairInfo.send_uid == uid) ? pairInfo.recv_uid : pairInfo.send_uid;
				if(pairInfo.last_message === undefined || pairInfo.last_message === '')
					return;
				pairInfo['opponent_uid'] = puser;
				pairInfo['key'] = subSnapshot.key;
				pairInfo['chat_type'] = 'pair';
				chatPairList.unshift(pairInfo);
			});
			updateChatPairList(chatPairList);
		});
	}

    componentWillUnmount() {
		clearInterval(this.timer);
        this.focusListener.remove();
	}
	
	updateChatList = () => {
		const { refresh } = this.state;
		this.setState({refresh:!refresh});
	}

	handleDeleteConversation = () => {
		const { user } = this.props;
		const uid = user.uid;
		const rowIndexx = this.state.activeRow;
		const chatPairInfo = this.chatList[rowIndexx];
		const pairId = chatPairInfo.pair_id;

		// update my chat pair info not to displayed in the chat list
		firebase.database().ref('chat_pair/' + uid + '/' + pairId).update({
			last_message: '',
		});

		// update chat messages
		firebase.database().ref('pair_message/' + pairId).once('value', function(snapshot) {
            snapshot.forEach(function(subSnapshot) {
				var key = subSnapshot.key;
				firebase.database().ref('pair_message/' + pairId + '/' + key).update({
					[uid]: true,
				})
            });
        });
	}

	getChatPairUnreadCount = (chatPairKey) => {
		const { user } = this.props;
		const uid = user.uid;
		return new Promise((resolve, reject) => {
			return firebase.database().ref('pair_message/' + chatPairKey).orderByChild('recv_uid').equalTo(uid).once('value')
			.then((snapshot) => {
				var unreadCount = 0;
				snapshot.forEach(function(subSnapshot) {
					var info = subSnapshot.val();
					const received = info.received ? info.received : false;
					if(!received) unreadCount++;
				});
				resolve(unreadCount);
			}, function(error) {
				reject(0);
			})
		})
	}

	getChatPairUserInfo = (chatPairInfo) => {
		var updateChatList = this.updateChatList;
		// this will remove the listener in chat room so comment out
		// firebase.database().ref('users/' + chatPairInfo.opponent_uid).off('value');
		firebase.database().ref('users/' + chatPairInfo.opponent_uid).on('value', snapshot => {
			const userInfo = snapshot.val();
			const username = (userInfo && userInfo.name) ? userInfo.name : '';
			const address = (userInfo && userInfo.address) ? userInfo.address : '';
			const avatar = (userInfo && userInfo.photo_url) ? userInfo.photo_url : null;
			chatPairInfo['username'] = username;
			chatPairInfo['address'] = address;
			chatPairInfo['avatar'] = avatar;
			updateChatList();
		})
	}

	updateChatPairList = async (chatPairList) => {
		for(var i=0;i<chatPairList.length;i++)
		{
			var chatPairInfo = chatPairList[i];
			await this.getChatPairUserInfo(chatPairInfo);
			chatPairInfo['unread'] = await this.getChatPairUnreadCount(chatPairInfo.key);
		}
		this.setState({chatPairList});
	}

	refreshChatPairListInfo = async () => {
		var { chatPairList } = this.state;
		for(var i=0;i<chatPairList.length;i++)
		{
			var chatPairInfo = chatPairList[i];
			await this.getChatPairUserInfo(chatPairInfo);
			chatPairInfo['unread'] = await this.getChatPairUnreadCount(chatPairInfo.key);
		}
		this.setState({chatPairList});
	}

	createChatClick = () => {
		this.props.navigation.navigate('ChatCreate');
	}

	openChatRoom = (pairId, opponentUid) => {
		this.props.navigation.navigate('ChatRoom', {
			pairId: pairId,
			opponentUid: opponentUid,
		});
	}

	openGroupChatRoom = (groupId, members) => {
		var memberList = Object.values(members);

		this.props.navigation.navigate('GroupChatRoom', {
			groupId: groupId,
			members: memberList,
		});
	}

	onSwipeOpen(rowId, direction) {
		if(typeof direction !== 'undefined'){
			this.setState({activeRow:rowId});
		}
	}
	
	onSwipeClose(rowId, direction) {
		if (rowId === this.state.activeRow && typeof direction !== 'undefined') {
			this.setState({ activeRow: null });
		}
	}


	_RenderChatPairItem = (item, index) => {
		return (
			<Swipeout right={this.btnsDefault}
				close={(this.state.activeRow !== index)}
				rowID={index}
				sectionId= {1}
				autoClose = {true}
				backgroundColor= 'transparent'
				sensitivity = {20}
				onOpen = {(secId, rowId, direction) => this.onSwipeOpen(rowId, direction)}
				onClose= {(secId, rowId, direction) => this.onSwipeClose(rowId, direction)}
				>
				<TouchableOpacity style={styles.chatContainer} onPress={() => this.openChatRoom(item.pair_id, item.opponent_uid)}>
					<FastImage
						style={styles.avatar}
						source={{
							uri: item.avatar,
							priority: FastImage.priority.high,
						}}
						resizeMode={FastImage.resizeMode.contain}
					/>
					<View style={styles.chatSubContainer}>
						<Text style={styles.nameText}>{item.username}</Text>
						{ item.type === 'text' ?
						<Text style={styles.messageText} numberOfLines={2}>{item.last_message}</Text>
						:
						<View style={{flexDirection:'row', alignItems:'center'}}>
							<FontAwesomeIcon name="photo" size={15} color={Colors.GREY} />
							<Text style={[styles.messageText, {marginLeft: 10}]}>Photo</Text>
						</View>
						}
					</View>
					<View style={{alignItems: 'center'}}>
						<Text style={styles.timestamp}>{timeAgoString(item.timestamp)}</Text>
						{ item.unread !== 0 ?
						<View style={{backgroundColor: Colors.PRIMARY_COLOR, width: 20, height: 20, borderRadius: 10, alignItems:'center'}}>
							<Text>{item.unread}</Text>
						</View>
						:
						<View />
						}
					</View>
				</TouchableOpacity>
			</Swipeout>
		)
	}

	_RenderChatGroupItem = (item, index) => {
		const last_message_info = item.last_message_info ? item.last_message_info : {};
		return (
			<Swipeout right={this.btnsDefault}
				close={(this.state.activeRow !== index)}
				rowID={index}
				sectionId= {1}
				autoClose = {true}
				backgroundColor= 'transparent'
				sensitivity = {20}
				onOpen = {(secId, rowId, direction) => this.onSwipeOpen(rowId, direction)}
				onClose= {(secId, rowId, direction) => this.onSwipeClose(rowId, direction)}
				>
				<TouchableOpacity style={styles.chatContainer} onPress={() => this.openGroupChatRoom(item.group_id, item.members)}>
					<FastImage
						style={styles.avatar}
						source={{
							uri: item.photo_url,
							priority: FastImage.priority.high,
						}}
						resizeMode={FastImage.resizeMode.contain}
					/>
					<View style={styles.chatSubContainer}>
						<Text style={styles.nameText}>{item.group_name}</Text>
						{ last_message_info && last_message_info.type === 'image' ?
						<View style={{flexDirection:'row', alignItems:'center'}}>
							<FontAwesomeIcon name="photo" size={15} color={Colors.GREY} />
							<Text style={[styles.messageText, {marginLeft: 10}]}>Photo</Text>
						</View>
						:
						<Text style={styles.messageText} numberOfLines={2}>{last_message_info && last_message_info.message ? last_message_info.message : ''}</Text>
						}
					</View>
					<View style={{alignItems: 'center'}}>
						<Text style={styles.timestamp}>{last_message_info.timestamp ? timeAgoString(last_message_info.timestamp) : ''}</Text>
						{ (item.unread !== 0 && item.unread !== undefined) ?
						<View style={{backgroundColor: Colors.PRIMARY_COLOR, width: 20, height: 20, borderRadius: 10, alignItems:'center'}}>
							<Text>{item.unread}</Text>
						</View>
						:
						<View />
						}
					</View>
				</TouchableOpacity>
			</Swipeout>
		)
	}

	combineTwoChatList = () => {
		const { chatPairList, chatGroupList } = this.state;
		var tmpChatPairList = chatPairList ? chatPairList : [];
		var tmpChatGroupList = chatGroupList ? chatGroupList : [];
		var chatList = [];
		chatList = tmpChatPairList.concat(tmpChatGroupList);
		
		chatList.sort((a,b) => {
			if (a.timestamp > b.timestamp)
				return -1;
			else if (a.timestamp < b.timestamp)
				return 1;
			else
				return 0;
		})
		return chatList;
	}

	render() {
		const { loading, refresh } = this.state;
		this.chatList = this.combineTwoChatList();

        if (loading)
            return (
                <View style={styles.loadingContainer}>
                    <ActivityIndicator size="large"/>
                </View>
			);
			
		return (
			<View style={{flex:1}}>
			<View style={styles.buttonContainer}>
				<TouchableOpacity style={styles.addChatButton} onPress={()=> this.createChatClick()}>
					<FontAwesomeIcon name="plus" size={40} color={Colors.WHITE_COLOR} />
				</TouchableOpacity>
			</View>
				{/* <ScrollView> */}
					<FlatList
						data={this.chatList}
						extraData={refresh}
						keyExtractor={(item, index) => {
							if(item.chat_type === 'group')
								return item.group_id;
							else
								return item.pair_id;
						}}
						renderItem={({item, index}) => {
							if(item.chat_type === 'group')
								return this._RenderChatGroupItem(item, index);
							else
								return this._RenderChatPairItem(item, index);
						}}
					/>
				{/* </ScrollView> */}
			</View>
		);
	}
}

const mapStateToProps = state => {
	return {
		user : state.UserReducer.user,
	};
};
  
const mapDispatchToProps = {
};
  
export default connect(mapStateToProps, mapDispatchToProps)(ChatList);


const styles = StyleSheet.create({
    loadingContainer: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: '#fff',
	},

	chatContainer: {
		flexDirection: 'row',
		alignItems: 'center',
		marginHorizontal: 10, 
		marginVertical: 15,
	},

	chatSubContainer: {
		width: width - 200,
		flexDirection: 'column',
		alignItems: 'flex-start',
		marginLeft: 20,
	},

	avatar: {
		width: 70,
		height: 70,
		borderRadius: 35,
	},

	timestamp: {
		marginLeft: 10,
		width: 80,
	},
	
	nameText: {
		fontSize: 18, 
		color: Colors.BLACK_COLOR,
	},
	
	messageText: {
		fontSize: 14, 
		color: Colors.GREY,
	},

	addChatButton: {
		width: 50,
		height: 50,
		borderRadius: 25,
		backgroundColor: Colors.PRIMARY_COLOR,
		justifyContent: 'center',
		alignItems: 'center',
	},

	buttonContainer: {
		position: 'absolute',
		justifyContent: 'flex-end',
		alignItems:'flex-end',
		right: 20,
		bottom: 20,
		zIndex: 50,
	}

});