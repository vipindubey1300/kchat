import React from 'react';
import {
	StyleSheet,
	Text,
	View,
	Alert,
    ActivityIndicator,
} from 'react-native';
import firebase from 'react-native-firebase';

import KeyboardView from '../../Components/KeyboardView';
import NavigationHeader from '../../Components/NavigationHeader';

import Colors from '../../Constants/Colors';

export default class NewsFeed extends React.Component {
	static navigationOptions = ({navigation, screenProps}) => ({
		title: 'Sign Up',
		header: null,
	});
	state = {
		loading: false,
	};

	constructor(props) {
		super();

		var navigation = props.navigation;
		var user = firebase.auth().currentUser;
		this.uid = user.uid;

		firebase.database().ref('users/' + this.uid).on('child_changed', function(data) {
			const key = data.key;
			const dbDeviceId = data.val();
			if(key === 'device_id' && dbDeviceId !== '' && dbDeviceId !== null && dbDeviceId !== undefined && dbDeviceId !== this.myDeviceId)
			{
				var currentUser = firebase.auth().currentUser;
				if(currentUser) {
					Alert.alert("Error", "Another user just signed in with your phone number. Please sign in again to use again.")
					firebase.auth().signOut();
					navigation.navigate('SignUpStackNavigation');
				}
			}
		});
	}

	componentWillUnmount() {
        firebase.database().ref('users/' + this.uid).off('child_changed');
	}

	componentDidMount() {
	}

	render() {
        const { loading } = this.state;

        if (loading)
            return (
                <View style={styles.loadingContainer}>
                    <ActivityIndicator size="large"/>
                </View>
			);
			
		return (
			<KeyboardView>
				<View>
					<NavigationHeader title="News Feed" haveBack={false} />
				</View>
				<View style={{flex:1, alignItems:'center'}}>
					<Text style={styles.descriptionText}>Please add NewsFeed Contents Here</Text>
				</View>
			</KeyboardView>
		);
	}
}

const styles = StyleSheet.create({
    loadingContainer: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: '#fff',
	},
	
	descriptionText: {
		textAlign:'center', 
		marginTop:40, 
		width:'70%', 
		fontSize: 16, 
		color: Colors.BLACK_COLOR
	},
});
