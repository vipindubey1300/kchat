import React from 'react';
import {
	StyleSheet,
	Text,
	View,
	Alert,
    ActivityIndicator,
} from 'react-native';

import KeyboardView from '../../Components/KeyboardView';
import NavigationHeader from '../../Components/NavigationHeader';

import Colors from '../../Constants/Colors';

export default class Photo extends React.Component {
	static navigationOptions = ({navigation, screenProps}) => ({
		title: 'Sign Up',
		header: null,
	});
	state = {
		loading: false,
	};

	constructor(props) {
		super();
	}

	componentDidMount() {
	}

	render() {
        const { loading } = this.state;

        if (loading)
            return (
                <View style={styles.loadingContainer}>
                    <ActivityIndicator size="large"/>
                </View>
			);
			
		return (
			<KeyboardView>
				<View>
					<NavigationHeader title="Photo" haveBack={false} />
				</View>
				<View style={{flex:1, alignItems:'center'}}>
					<Text style={styles.descriptionText}>Please add Photo Contents Here</Text>
				</View>
			</KeyboardView>
		);
	}
}

const styles = StyleSheet.create({
    loadingContainer: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: '#fff',
	},
	
	descriptionText: {
		textAlign:'center', 
		marginTop:40, 
		width:'70%', 
		fontSize: 16, 
		color: Colors.BLACK_COLOR
	},
});
