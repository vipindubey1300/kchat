import React from 'react';
import {
	StyleSheet,
	Text,
	View,
	Alert,
    ActivityIndicator,
} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import CountryPicker, {
	getAllCountries
} from '../../Components/CountryPicker/src/CountryPicker';
import firebase from 'react-native-firebase';

import KeyboardView from '../../Components/KeyboardView';
import NavigationHeader from '../../Components/NavigationHeader';
import PhoneNumberInput from '../../Components/TextInput/PhoneNumberInput';

import Colors from '../../Constants/Colors';

export default class SignUpPhoneNumber extends React.Component {
	static navigationOptions = ({navigation, screenProps}) => ({
		title: 'Sign Up',
		header: null,
	});

	constructor(props) {
		super(props);

		let userLocaleCountryCode = DeviceInfo.getDeviceCountry();
		this.allCountries = getAllCountries();

		const userCountryData = this.allCountries
			.filter(country => country.cca2 === userLocaleCountryCode)
			.pop();

		let callingCode = null;
		let cca2 = userLocaleCountryCode;
		if (!cca2 || !userCountryData) {
			cca2 = 'US';
			callingCode = '1';
		} else {
			callingCode = userCountryData.callingCode;
		}
		
		this.state = {
			cca2,
			callingCode,
		};
	}

	componentDidMount() {
		this.phoneNumberInput.setState({areaCode: "+" + this.state.callingCode});
	}

	onNextClick = () => {
		const { callingCode } = this.state;
		const localNumber = this.phoneNumberInput.getInputValue();

		if(callingCode === ""){
			Alert.alert("Error", 'Please Select Country!');
			return;
		}

		if(localNumber === ""){
			Alert.alert("Error", 'Please Enter Phone Number!');
			return;
		}

		const phoneNumber = "+" + callingCode + localNumber;

		this.props.navigation.navigate('SignUpVerification', {
			phoneNumber,
		});
	}

	phoneNumberChange = (text) => {
		this.phoneNumberInput.setState({text})
	}

	render() {
        const { loading } = this.state;

        if (loading)
            return (
                <View style={styles.loadingContainer}>
                    <ActivityIndicator size="large"/>
                </View>
			);
			
		return (
			<KeyboardView>
				<View>
					<NavigationHeader title="Sign Up" showMark={true} haveBack={false} rightButtonLabel="Next" rightButtonAction={this.onNextClick.bind(this)}/>
				</View>
				<View style={{flex:1, alignItems:'center'}}>
					<Text style={styles.descriptionText}>Please confirm your country code and entry your mobile number</Text>
					<CountryPicker
						showCountryNameWithFlag={true}
						showCallingCode={true}
						hideAlphabetFilter={true}
						filterable={true}
						onChange={value => {
							this.setState({ cca2: value.cca2, callingCode: value.callingCode });
							this.phoneNumberInput.setState({areaCode: "+" + value.callingCode});
						}}
						cca2={this.state.cca2}
						translation="eng"
					/>
					<PhoneNumberInput
						placeholder="Enter Mobile Number"
						keyboardType="phone-pad"
						style={styles.phoneNumberInput}
						error={false}
						// focus={this.onChangeInputFocus}
						ref={ref => this.phoneNumberInput = ref}
						returnKeyType="done"
						defaultValue=""
						onChangeText={(text) => this.phoneNumberChange(text)}
					/>
				</View>
			</KeyboardView>
		);
	}
}

const styles = StyleSheet.create({
    loadingContainer: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: '#fff',
	},
	
	descriptionText: {
		textAlign:'center', 
		marginTop:40, 
		width:'70%', 
		fontSize: 16, 
		color: Colors.BLACK_COLOR
	},

	phoneNumberInput: {
		marginTop: 20,
		width: '90%',
	},
});
