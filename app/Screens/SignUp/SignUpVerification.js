import React from 'react';
import {
	StyleSheet,
	Text,
	View,
	Alert,
	ActivityIndicator,
	TouchableOpacity
} from 'react-native';
import firebase from 'react-native-firebase';
import DeviceInfo from 'react-native-device-info';
import KeyboardView from '../../Components/KeyboardView';
import NavigationHeader from '../../Components/NavigationHeader';
import VerificationCodeInput from '../../Components/TextInput/VerificationCodeInput';

import Colors from '../../Constants/Colors';
import { getFCMToken, registerUserFCMToken, unregisterUserFCMToken } from '../../Utils/NotificationUtil';

import { connect } from 'react-redux';
import { setUser, fetchDBProfile, updateDBUserDeviceID } from '../../Actions/UserActions';

class SignUpVerification extends React.Component {
	static navigationOptions = ({navigation, screenProps}) => ({
		title: 'Sign Up',
		header: null,
	});

	state = {
		loading: false,
	};

	constructor(props) {
		super(props);

        this.phoneNumber  = this.props.navigation.getParam('phoneNumber', '');
		this.verificationId  = null;
		
		this.myDeviceId = DeviceInfo.getUniqueID();
	}

	componentDidMount() {
		this.setState({loading:true})
		var handleVerficationSuccess = this.handleVerficationSuccess;
		this.unsubscribe = firebase.auth().onAuthStateChanged((user) => {
            if (user) {
				handleVerficationSuccess(user)
            } else {

            }
		});
		
		firebase.auth()
		.verifyPhoneNumber(this.phoneNumber)
		.on('state_changed', (phoneAuthSnapshot) => {
			// How you handle these state events is entirely up to your ui flow and whether
			// you need to support both ios and android. In short: not all of them need to
			// be handled - it's entirely up to you, your ui and supported platforms.

			// E.g you could handle android specific events only here, and let the rest fall back
			// to the optionalErrorCb or optionalCompleteCb functions
			switch (phoneAuthSnapshot.state) {
			// ------------------------
			//  IOS AND ANDROID EVENTS
			// ------------------------
			case firebase.auth.PhoneAuthState.CODE_SENT: // or 'sent'
				console.log('code sent');
				Alert.alert("Info", "Verification has been sent.");
				this.setState({loading:false});
				// on ios this is the final phone auth state event you'd receive
				// so you'd then ask for user input of the code and build a credential from it
				break;
			case firebase.auth.PhoneAuthState.ERROR: // or 'error'
				console.log('verification error');
				console.log(phoneAuthSnapshot.error);
				break;

			// ---------------------
			// ANDROID ONLY EVENTS
			// ---------------------
			case firebase.auth.PhoneAuthState.AUTO_VERIFY_TIMEOUT: // or 'timeout'
				console.log('auto verify on android timed out');
				// proceed with your manual code input flow, same as you would do in
				// CODE_SENT if you were on IOS
				break;
			case firebase.auth.PhoneAuthState.AUTO_VERIFIED: // or 'verified'
				// auto verified means the code has also been automatically confirmed as correct/received
				// phoneAuthSnapshot.code will contain the auto verified sms code - no need to ask the user for input.
				console.log('auto verified on android');
				console.log(phoneAuthSnapshot);
				// Example usage if handling here and not in optionalCompleteCb:
				// const { verificationId, code } = phoneAuthSnapshot;
				// const credential = firebase.auth.PhoneAuthProvider.credential(verificationId, code);

				// Do something with your new credential, e.g.:
				// firebase.auth().signInWithCredential(credential);
				// firebase.auth().linkWithCredential(credential);
				// etc ...
				break;
			}
		}, (error) => {
			// optionalErrorCb would be same logic as the ERROR case above,  if you've already handed
			// the ERROR case in the above observer then there's no need to handle it here
			console.log(error);
			// verificationId is attached to error if required
			console.log(error.verificationId);
			this.verificationId = error.verificationId;
			Alert.alert("Error", error.nativeErrorMessage);
			this.setState({loading:false});
		}, (phoneAuthSnapshot) => {
			// optionalCompleteCb would be same logic as the AUTO_VERIFIED/CODE_SENT switch cases above
			// depending on the platform. If you've already handled those cases in the observer then
			// there's absolutely no need to handle it here.

			// Platform specific logic:
			// - if this is on IOS then phoneAuthSnapshot.code will always be null
			// - if ANDROID auto verified the sms code then phoneAuthSnapshot.code will contain the verified sms code
			//   and there'd be no need to ask for user input of the code - proceed to credential creating logic
			// - if ANDROID auto verify timed out then phoneAuthSnapshot.code would be null, just like ios, you'd
			//   continue with user input logic.
			console.log(phoneAuthSnapshot);
			if(phoneAuthSnapshot.code === null) {
				const { verificationId, code } = phoneAuthSnapshot;
				this.verificationId = verificationId;
				this.setState({loading:false});
			}
			else {	// auto verified
				const { verificationId, code } = phoneAuthSnapshot;
				const credential = firebase.auth.PhoneAuthProvider.credential(verificationId, code);

				firebase.auth().signInWithCredential(credential);
			}
		});
	}

	handleVerficationSuccess = async (user) => {
		this.props.setUser({
			uid: user.uid,
			phoneNumber: user.phoneNumber,
		});

		const fcmToken = await getFCMToken();
		await registerUserFCMToken(user.uid, fcmToken);
		this.props.updateDBUserDeviceID(user.uid, this.myDeviceId);

		new Promise((resolve, reject) => {
			this.props.fetchDBProfile(
				user.uid,
				resolve,
				reject,
			)
		})
		.then( result => {
			this.setState({ loading: false });
			if(this.props.profile === null || this.props.profile.phone_number === undefined || this.props.profile.phone_number === null || this.props.profile.phone_number === ''
				|| this.props.profile.name === undefined || this.props.profile.name === null || this.props.profile.name === ''
				|| this.props.profile.photo_url === undefined || this.props.profile.photo_url === null || this.props.profile.photo_url === ''
				|| this.props.profile.address === undefined || this.props.profile.address === null || this.props.profile.address === '')
			{
				this.props.navigation.navigate('SignUpProfile', {
					phoneNumber: this.phoneNumber,
					uid: user.uid,
				});
			}
			else
				this.props.navigation.navigate('MainStackNavigation')
		})
		.catch( error => {
			this.setState({ loading: false });
			console.log('Failed To Get Profile Information!')
			this.props.navigation.navigate('SignUpProfile', {
				phoneNumber: this.phoneNumber,
				uid: user.uid,
			});
		})
	}

	onNextClick = () => {
		const verficationCode = this.verificationCodeInput.getInputValue();
		var handleVerficationSuccess = this.handleVerficationSuccess;

		if(this.verificationId === null ){
			Alert.alert("Error", 'Please Resend SMS!');
			return;
		}

		if(verficationCode === ''){
			Alert.alert("Error", 'Please Enter Verification Code!');
			return;
		}

		this.setState({loading:true})		
		const credential = firebase.auth.PhoneAuthProvider.credential(this.verificationId, verficationCode);
		firebase.auth().signInWithCredential(credential)
		.then((user) => {
			console.log("sign in success")
		}).catch((err) => {
			this.setState({loading:false});
		})
	}

	resendSMS = () => {
		this.setState({loading:true})
		firebase.auth()
		.verifyPhoneNumber(this.phoneNumber)
		.on('state_changed', (phoneAuthSnapshot) => {
			// How you handle these state events is entirely up to your ui flow and whether
			// you need to support both ios and android. In short: not all of them need to
			// be handled - it's entirely up to you, your ui and supported platforms.

			// E.g you could handle android specific events only here, and let the rest fall back
			// to the optionalErrorCb or optionalCompleteCb functions
			switch (phoneAuthSnapshot.state) {
			// ------------------------
			//  IOS AND ANDROID EVENTS
			// ------------------------
			case firebase.auth.PhoneAuthState.CODE_SENT: // or 'sent'
				console.log('code sent');
				Alert.alert("Info", "Verification has been sent.");
				// on ios this is the final phone auth state event you'd receive
				// so you'd then ask for user input of the code and build a credential from it
				break;
			case firebase.auth.PhoneAuthState.ERROR: // or 'error'
				console.log('verification error');
				console.log(phoneAuthSnapshot.error);
				break;

			// ---------------------
			// ANDROID ONLY EVENTS
			// ---------------------
			case firebase.auth.PhoneAuthState.AUTO_VERIFY_TIMEOUT: // or 'timeout'
				console.log('auto verify on android timed out');
				// proceed with your manual code input flow, same as you would do in
				// CODE_SENT if you were on IOS
				break;
			case firebase.auth.PhoneAuthState.AUTO_VERIFIED: // or 'verified'
				// auto verified means the code has also been automatically confirmed as correct/received
				// phoneAuthSnapshot.code will contain the auto verified sms code - no need to ask the user for input.
				console.log('auto verified on android');
				console.log(phoneAuthSnapshot);
				// Example usage if handling here and not in optionalCompleteCb:
				// const { verificationId, code } = phoneAuthSnapshot;
				// const credential = firebase.auth.PhoneAuthProvider.credential(verificationId, code);

				// Do something with your new credential, e.g.:
				// firebase.auth().signInWithCredential(credential);
				// firebase.auth().linkWithCredential(credential);
				// etc ...
				break;
			}
		}, (error) => {
			// optionalErrorCb would be same logic as the ERROR case above,  if you've already handed
			// the ERROR case in the above observer then there's no need to handle it here
			console.log(error);
			// verificationId is attached to error if required
			console.log(error.verificationId);
			this.verificationId = error.verificationId;
			Alert.alert("Error", error.nativeErrorMessage);
		}, (phoneAuthSnapshot) => {
			// optionalCompleteCb would be same logic as the AUTO_VERIFIED/CODE_SENT switch cases above
			// depending on the platform. If you've already handled those cases in the observer then
			// there's absolutely no need to handle it here.

			// Platform specific logic:
			// - if this is on IOS then phoneAuthSnapshot.code will always be null
			// - if ANDROID auto verified the sms code then phoneAuthSnapshot.code will contain the verified sms code
			//   and there'd be no need to ask for user input of the code - proceed to credential creating logic
			// - if ANDROID auto verify timed out then phoneAuthSnapshot.code would be null, just like ios, you'd
			//   continue with user input logic.
			console.log(phoneAuthSnapshot);
			if(phoneAuthSnapshot.code === null) {
				const { verificationId, code } = phoneAuthSnapshot;
				this.verificationId = verificationId;
				this.setState({loading:false});
			}
			else {	// auto verified
				const { verificationId, code } = phoneAuthSnapshot;
				const credential = firebase.auth.PhoneAuthProvider.credential(verificationId, code);

				firebase.auth().signInWithCredential(credential);
			}
		});
	}

	render() {
        const { loading } = this.state;

        if (loading)
            return (
                <View style={styles.loadingContainer}>
                    <ActivityIndicator size="large"/>
                </View>
			);
			
		return (
			<KeyboardView>
				<View>
					<NavigationHeader title="Sign Up" showMark={true} haveBack={true} rightButtonLabel="Next" rightButtonAction={this.onNextClick.bind(this)}/>
				</View>
				<View style={{flex:1, alignItems:'center'}}>
					<Text style={styles.titleText}>Verify</Text>

					<Text style={styles.phoneNumberText}>{this.phoneNumber}</Text>

					<TouchableOpacity style={{marginTop:10,}} onPress={() => this.props.navigation.goBack()}>
						<Text style={styles.resendText}>Wrong Number?</Text>
					</TouchableOpacity>

					<VerificationCodeInput
						placeholder="Enter the 6 Digit Code"
						keyboardType="number-pad"
						style={styles.verificationCodeInput}
						error={false}
						// focus={this.onChangeInputFocus}
						ref={ref => this.verificationCodeInput = ref}
						returnKeyType="done"
						defaultValue=""
					/>
					
					<TouchableOpacity style={{marginTop:40,}} onPress={() => this.resendSMS()}>
						<Text style={styles.resendText}>Resend SMS</Text>
					</TouchableOpacity>
				</View>
			</KeyboardView>
		);
	}
}

const mapStateToProps = state => {
	return {
		user: state.UserReducer.user,
		profile: state.UserReducer.profile,
	};
};
  
const mapDispatchToProps = {
	setUser,
	fetchDBProfile,
	updateDBUserDeviceID
};
  
export default connect(mapStateToProps, mapDispatchToProps)(SignUpVerification);

const styles = StyleSheet.create({
    loadingContainer: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: '#fff',
	},
	
	titleText: {
		marginTop: 40,
		color: 'black',
		fontSize: 18, 
		fontWeight:'500',
		textAlign: 'center',
	},

	verificationCodeInput: {
		marginTop: 40,
	},
	
	phoneNumberText: {
		textAlign:'center', 
		marginTop:40, 
		fontSize: 26, 
		color: Colors.BLACK_COLOR
	},
	
	resendText: {
		color:Colors.BLUE_COLOR, 
		borderBottomColor: Colors.BLUE_COLOR,
		borderBottomWidth: 1,
	},
});
