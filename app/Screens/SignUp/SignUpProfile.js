import React from 'react';
import {
	StyleSheet,
	Text,
	View,
	Alert,
	ActivityIndicator,
	TouchableOpacity
} from 'react-native';
import FastImage from 'react-native-fast-image'
import Dialog, {
    DialogContent,
    SlideAnimation,
} from 'react-native-popup-dialog';
import firebase from 'react-native-firebase';
import ImagePicker from 'react-native-image-crop-picker';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome'

import KeyboardView from '../../Components/KeyboardView';
import NavigationHeader from '../../Components/NavigationHeader';
import UnderlineTextInput from '../../Components/TextInput/UnderlineTextInput';

import Colors from '../../Constants/Colors';
import { UPLOAD_AVATAR } from '../../Utils/UploadUtil'

import { connect } from 'react-redux';
import { updateDBProfile } from '../../Actions/UserActions';

class SignUpProfile extends React.Component {
	static navigationOptions = ({navigation, screenProps}) => ({
		title: 'Sign Up',
		header: null,
	});

	state = {
		loading: false,
		photoSelectDialogShow: false,
		photo: null,
		name: null,
		address: null,
	};

	constructor(props) {
		super(props);

        this.phoneNumber  = this.props.navigation.getParam('phoneNumber', '');
        this.uid  = this.props.navigation.getParam('uid', '');
	}

	componentDidMount() {
	}

	onSaveClick = () => {
		const { updateDBProfile }  = this.props;		
		const { photo } = this.state;
		const phoneNumber = this.phoneNumber;
		const name = this.nameInput.getInputValue();
		const address = this.addressInput.getInputValue();

		if(photo === null ){
			Alert.alert("Error", 'Please Select a Photo!');
			return;
		}

		if(name === '' ){
			Alert.alert("Error", 'Please Enter a Name!');
			return;
		}

		if(address === '' ){
			Alert.alert("Error", 'Please Enter a Address!');
			return;
		}

		new Promise((resolve, reject) => {
			this.props.updateDBProfile(
				{
					photo_url: photo, 
					name: name,
					address: address,
					phone_number: phoneNumber,
				},
				resolve,
				reject,
			)
		})
		.then( success => {
			this.props.navigation.navigate('MainStackNavigation')
		})
		.catch( error => {
			console.log(error)
			Alert.alert("Error", 'Failed To Save Profile Information, Please try Again!')
		})
	}

	onAddPhotoClick = () => {
        this.setState({photoSelectDialogShow:true});
	}

    pickSingleFromCamera = () => {
        ImagePicker.openCamera({
            cropping: true,
            width: 300,
            height: 300,
            includeExif: true,
        }).then(async (image) => {
			this.setState({loading:true});
            var photo = await UPLOAD_AVATAR(image.path, this.uid, 'profile', 'image/jpg');
			this.setState({photo, loading:false});
        }).catch(e => {
            console.log(e);
        });
    }

    pickSingleFromGallery = () => {
        ImagePicker.openPicker({
            width: 300,
            height: 300,
            cropping: true,
            includeExif: true,
        }).then(async (image) => {
			this.setState({loading:true});
            var photo = await UPLOAD_AVATAR(image.path, this.uid, 'profile', 'image/jpg');
			this.setState({photo, loading:false});
        }).catch(e => {
            console.log(e);
        });
	}

	handleNameChange = (text) => {
		this.nameInput.setState({text});
		this.setState({name:text});
	}

	handleAddressChange = (text) => {
		this.addressInput.setState({text});
		this.setState({address:text});
	}
	
	render() {
		const { loading, photo, name, address } = this.state;
		const { isFetching } = this.props;
        if (loading || isFetching)
            return (
                <View style={styles.loadingContainer}>
                    <ActivityIndicator size="large"/>
                </View>
			);
			
		return (
			<KeyboardView>
				<View>
					<NavigationHeader title="Create Profile" showMark={false} haveBack={true} rightButtonLabel="Save" rightButtonAction={this.onSaveClick.bind(this)}/>
				</View>
				<View style={{margin: 20,}}>
					<View style={{alignSelf: 'center'}} >
						<TouchableOpacity style={styles.takePhotoButton} onPress={() => this.onAddPhotoClick()}>
							<FontAwesomeIcon name="instagram" size={25} color={Colors.WHITE_COLOR} style={{textAlign:'center'}}/>
						</TouchableOpacity>
						<FastImage
							style={styles.avatar}
							source={{
								uri: photo ? photo : "https://www.uog.edu.gy/sites/default/files/staff/council_member_3.png",
								priority: FastImage.priority.normal,
							}}
							resizeMode={FastImage.resizeMode.contain}
						/>
					</View>
					
					<UnderlineTextInput
						placeholder="Name"
						keyboardType="default"
						style={styles.nameInput}
						error={false}
						// focus={this.onChangeInputFocus}
						ref={ref => this.nameInput = ref}
						returnKeyType="done"
						defaultValue={name ? name : ''}
						onChangeText={(text) => this.handleNameChange(text)}
					/>

					<UnderlineTextInput
						placeholder="City, State"
						keyboardType="default"
						style={styles.addressInput}
						error={false}
						// focus={this.onChangeInputFocus}
						ref={ref => this.addressInput = ref}
						returnKeyType="done"
						defaultValue={address ? address : ''}
						onChangeText={(text) => this.handleAddressChange(text)}
					/>

					<UnderlineTextInput
						placeholder="Mobile Number"
						keyboardType="default"
						style={styles.phoneNumberInput}
						error={false}
						// focus={this.onChangeInputFocus}
						ref={ref => this.phoneNumberInput = ref}
						returnKeyType="done"
						defaultValue={this.phoneNumber}
						editable={false}
					/>					
				</View>

				{/* Photo Selection Dialog */}
                <Dialog
                    onDismiss={() => this.setState({ photoSelectDialogShow: false })}
                    onTouchOutside={() => this.setState({ photoSelectDialogShow: false })}
                    onHardwareBackPress={() => this.setState({ photoSelectDialogShow: false })}
                    rounded={false}
                    width={0.9}
                    visible={this.state.photoSelectDialogShow}
                    dialogAnimation={new SlideAnimation({
                        slideFrom: 'bottom',
                    })}>
                    <DialogContent
                        style={{backgroundColor: 'white',}}>
                        <View style={{flexDirection:'column', alignItems: 'center', marginTop:20,}}>              
                            <Text style={{fontSize:18, color: Colors.BLACK_COLOR, marginBottom: 10,}}>Select Photo from</Text>
                            <View style={styles.dialogButtonGroupStyle}>
                                <TouchableOpacity style={styles.dialogButtonStyle}
                                onPress={() => {this.setState({ photoSelectDialogShow: false }); this.pickSingleFromGallery();}}>
                                    <Text style={{fontSize:16,color:'black',textAlign:'center',}}>From Gallery</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.dialogButtonStyle}
                                onPress={() => {this.setState({ photoSelectDialogShow: false }); this.pickSingleFromCamera();}}>
                                    <Text style={{fontSize:16,color:'black',textAlign:'center',}}>From Camera</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </DialogContent>
                </Dialog>
			</KeyboardView>
		);
	}
}

const mapStateToProps = state => {
	return {
		profile : state.UserReducer.profile,
		user: state.UserReducer.user,
		isFetching: state.UserReducer.isFetching,
		isSuccess: state.UserReducer.isSuccess,
	};
};
  
const mapDispatchToProps = {
	updateDBProfile,
};
  
export default connect(mapStateToProps, mapDispatchToProps)(SignUpProfile);

const styles = StyleSheet.create({
    loadingContainer: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: '#fff',
	},
	
	titleText: {
		marginTop: 40,
		color: 'black',
		fontSize: 18, 
		fontWeight:'500',
		textAlign: 'center',
	},

	avatar: {
		width: 150,
		height: 150,
		borderRadius: 75, 
		borderWidth: 1,
		borderColor: Colors.BLACK_COLOR,
		zIndex: 1,
	},

	nameInput: {
		marginTop: 20,
	},

	addressInput: {
		marginTop: 20,
	},

	phoneNumberInput: {
		marginTop: 20,
	},

	takePhotoButton: {
		width: 40, 
		height: 40, 
		position: 'absolute',
		backgroundColor:'rgba(0,0,0,0.5)',
		borderRadius: 20,
		justifyContent: 'center',
		top: 70,
		bottom: 0,
		left: 130,
		right: 0,
		zIndex: 2,
	},

    dialogButtonGroupStyle: {
		flexDirection: 'row',
		marginTop: 10,
		height: 50,
		alignItems: 'center',
    },
  
    dialogButtonStyle: {
		height: 50, 
		width: '45%',
		justifyContent: 'center', 
		alignItems: 'center',
		backgroundColor: 'white', 
		borderColor: 'gray', 
		borderWidth: 1, 
		marginRight: 10,
    },
});
