import { fork, all } from 'redux-saga/effects';

import UserSaga from './UserSaga'
import ChatSaga from './ChatSaga'

export default function* rootSaga() {
  yield all([
    fork(UserSaga),
    fork(ChatSaga),
  ]);
}
