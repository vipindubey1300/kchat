import { put, call, takeEvery, select, take, fork, all } from 'redux-saga/effects'
import firebase from 'react-native-firebase';
import * as actions from '../Actions/UserActions'
import {
	userSelector,
} from "../Reducers/Selectors";

function fetchDBProfileApi(uid) {
	return firebase.database().ref('users/' + uid).once('value')
	.then((snapshot) => {
		console.log('User profile successfully fetched.');
		return snapshot.val();
	})
	.catch((error) => {
		console.error("Error fetching user profile: ", error);
		return null;
	});
}

function* fetchDBProfile() {
	while(true)
	{
		const action = yield take(actions.FETCH_DB_PROFILE);
		yield put(actions.APIRequest());
		const profile = yield call(fetchDBProfileApi, action.uid);
		yield put(actions.APIResponse(true));
		yield put(actions.receiveProfile(profile));
		if(profile != null)
			action.resolve(profile)
		else
			action.reject(profile)
	}
}

function updateDBProfileApi(data) {
	const uid = data.uid;
	const profile = data.profile;

	return firebase.database().ref('users/' + uid).update(profile)
	.then(function() {
		console.log('User profile successfully updated.');
		return true;
	})
	.catch(function(error) {
		console.error("Error updating user profile: ", error);
		return false;
	});
}

function* updateDBProfile() {
	while(true)
	{
		const action = yield take(actions.UPDATE_DB_PROFILE);
		const user = yield select(userSelector);
		yield put(actions.APIRequest());
		const success = yield call(updateDBProfileApi, {
			profile: action.profile,
			uid: user.uid,
		});
		yield put(actions.APIResponse(success));
		yield put(actions.receiveProfile(action.profile));
		if(success)
			action.resolve("Success")
		else
			action.reject("Failed")
	}
}

function updateDBUserDeviceIDApi(data) {
	const uid = data.uid;
	const deviceId = data.deviceId;

	return firebase.database().ref('users/' + uid).update({
		device_id: deviceId
	})
	.then(function() {
		console.log('User device successfully updated.');
		return true;
	})
	.catch(function(error) {
		console.error("Error updating user device: ", error);
		return false;
	});
}

function* updateDBUserDeviceID() {
	while(true)
	{
		const action = yield take(actions.UPDATE_DB_USER_DEVICE_ID);
		yield call(updateDBUserDeviceIDApi, {
			uid: action.uid,
			deviceId: action.deviceId,
		});
	}
}


function fetchDBUserListApi() {
	return firebase.database().ref('users').once('value')
	.then((snapshot) => {
		var userList = [];
		snapshot.forEach(function(subSnapshot) {
			var userInfo = subSnapshot.val();
			userInfo['uid'] = subSnapshot.key;
			userList.unshift(userInfo);
		});

		console.log('User List successfully fetched.');
		return userList;
	})
	.catch((error) => {
		console.error("Error fetching user list: ", error);
		return null;
	});
}

function* fetchDBUserList() {
	while(true)
	{
		const action = yield take(actions.FETCH_DB_USER_LIST);
		yield put(actions.APIRequest());
		const userList = yield call(fetchDBUserListApi);
		yield put(actions.APIResponse(true));
		yield put(actions.receiveUserList(userList));
		if(userList != null)
			action.resolve(userList)
		else
			action.reject(userList)
	}
}

export default function* UserSaga() {
	yield all([
		fork(updateDBProfile),
		fork(fetchDBProfile),
		fork(updateDBUserDeviceID),
		fork(fetchDBUserList),
	]);
}