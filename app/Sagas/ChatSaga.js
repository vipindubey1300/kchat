import { put, call, takeEvery, select, take, fork, all } from 'redux-saga/effects'
import firebase from 'react-native-firebase';
import * as actions from '../Actions/ChatActions'
import {
	userSelector,
} from "../Reducers/Selectors";
function fetchDBChatPairUnreadCountApi(uid, chatPairKey) {
	return new Promise((resolve, reject) => {
		return firebase.database().ref('pair_message/' + chatPairKey).orderByChild('recv_uid').equalTo(uid).once('value')
		.then((snapshot) => {
			var unreadCount = 0;
			snapshot.forEach(function(subSnapshot) {
				var info = subSnapshot.val();
				const received = info.received ? info.received : false;
				if(!received) unreadCount++;
			});
			resolve(unreadCount);
		}, function(error) {
			reject(0);
		})
	})
}

function fetchDBChatPairListApi(uid) {
	return firebase.database().ref('chat_pair/' + uid).orderByChild('timestamp').once('value')
	.then(function(snapshot) {
		var chatPairList = [];
		snapshot.forEach(function(subSnapshot) {
			var pairInfo = subSnapshot.val();
			var puser = (pairInfo.send_uid == uid) ? pairInfo.recv_uid : pairInfo.send_uid;
			if(pairInfo.last_message === undefined || pairInfo.last_message === '')
				return;
			pairInfo['opponent_uid'] = puser;
			pairInfo['key'] = subSnapshot.key;
			pairInfo['type'] = 'pair';
			chatPairList.unshift(pairInfo);
		});
		return chatPairList;
	}, function(error) {
		console.log(error);
		return [];
	});
}

function fetchDBChatGroupListApi(uid) {
	return firebase.database().ref('chat_group').orderByChild('timestamp').once('value')
	.then(function(snapshot) {
		var chatGroupList = [];
		snapshot.forEach(function(subSnapshot) {
			var groupInfo = subSnapshot.val();
			groupInfo['type'] = 'group';
			groupInfo['key'] = subSnapshot.key;
			groupInfo['timestamp'] = groupInfo['last_message_info'] && groupInfo['last_message_info'].timestamp ? groupInfo['last_message_info'].timestamp : 0;
			var members = groupInfo.members ? groupInfo.members : {};
			if(!members.hasOwnProperty(uid)) return;
			chatGroupList.unshift(groupInfo);
		});
		return chatGroupList;
	}, function(error) {
		console.log(error);
		return [];
	});
}

function insertUserInfoToChatPairListApi(value) {
	return firebase.database().ref('users/' + value.opponent_uid).once('value')
		.then(function(snapshot) {
			const userInfo = snapshot.val();
			const username = (userInfo && userInfo.name) ? userInfo.name : '';
			const address = (userInfo && userInfo.address) ? userInfo.address : '';
			const avatar = (userInfo && userInfo.photo_url) ? userInfo.photo_url : null;
			value['username'] = username;
			value['address'] = address;
			value['avatar'] = avatar;
			return value
		}, function(error) {
			console.error(error);
			return value
		});
}

function* fetchDBChatPairList() {
	while(true)
	{
		const action = yield take(actions.FETCH_DB_CHAT_PAIR_LIST);
		yield put(actions.APIRequest());
		let chatPairList = yield call(fetchDBChatPairListApi, action.uid);
		for(var i=0;i<chatPairList.length;i++)
		{
			let value = yield call(insertUserInfoToChatPairListApi, chatPairList[i]);
			value['unread'] = yield call(fetchDBChatPairUnreadCountApi, action.uid, value.key);
			chatPairList[i] = value;
		}
		yield put(actions.APIResponse(chatPairList.length != 0));
		yield put(actions.receiveChatPairList(chatPairList));
		if(chatPairList.length != 0)
			action.resolve(chatPairList)
		else
			action.reject(chatPairList)
	}
}


function* fetchDBChatGroupList() {
	while(true)
	{
		const action = yield take(actions.FETCH_DB_CHAT_GROUP_LIST);
		yield put(actions.APIRequest());
		let chatGroupList = yield call(fetchDBChatGroupListApi, action.uid);
		yield put(actions.APIResponse(chatGroupList.length != 0));
		yield put(actions.receiveChatGroupList(chatGroupList));
		if(chatGroupList.length != 0)
			action.resolve(chatGroupList)
		else
			action.reject(chatGroupList)
	}
}

export default function* ChatSaga() {
	yield all([
		fork(fetchDBChatPairList),
		fork(fetchDBChatGroupList),
	]);
}