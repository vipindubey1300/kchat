import { applyMiddleware, createStore, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from '../Reducers';
import rootSaga from '../Sagas';
export const sagaMiddleware = createSagaMiddleware();

const middlewares = [
	sagaMiddleware,
];

export const store = createStore(
	rootReducer,
	compose(applyMiddleware(...middlewares)),
);

sagaMiddleware.run(rootSaga)