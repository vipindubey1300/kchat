export const API_REQUEST = "API/API_REQUEST"
export const API_RESPONSE = "API/API_RESPONSE"

export const FETCH_DB_CHAT_PAIR_LIST = "CHAT/FETCH_DB_CHAT_PAIR_LIST"
export const RECEIVE_CHAT_PAIR_LIST = "CHAT/RECEIVE_CHAT_PAIR_LIST"

export const FETCH_DB_CHAT_GROUP_LIST = "CHAT/FETCH_DB_CHAT_GROUP_LIST"
export const RECEIVE_CHAT_GROUP_LIST = "CHAT/RECEIVE_CHAT_GROUP_LIST"

export function APIRequest() {
    return {
        type: API_REQUEST,
    }
}

export function APIResponse(result) {
    return {
        type: API_RESPONSE,
        result,
    }
}

export function fetchDBChatPairList(uid, resolve, reject) {
    return {
        type: FETCH_DB_CHAT_PAIR_LIST,
        uid,
        resolve,
        reject,
    }
}

export function fetchDBChatGroupList(uid, resolve, reject) {
    return {
        type: FETCH_DB_CHAT_GROUP_LIST,
        uid,
        resolve,
        reject,
    }
}

export function receiveChatPairList(chatPairList) {
    return {
        type: RECEIVE_CHAT_PAIR_LIST,
        chatPairList,
    }
}

export function receiveChatGroupList(chatGroupList) {
    return {
        type: RECEIVE_CHAT_GROUP_LIST,
        chatGroupList,
    }
}