export const API_REQUEST = "API/API_REQUEST"
export const API_RESPONSE = "API/API_RESPONSE"

export const SET_USER = "USER/SET_USER"
export const REMOVE_USER = "USER/REMOVE_USER"
export const UPDATE_DB_USER_DEVICE_ID = "USER/UPDATE_DB_USER_DEVICE_ID"
export const FETCH_DB_PROFILE = "USER/FETCH_DB_PROFILE"
export const UPDATE_DB_PROFILE = "USER/UPDATE_DB_PROFILE"
export const RECEIVE_PROFILE = "USER/RECEIVE_PROFILE"
export const FETCH_DB_USER_LIST = "USER/FETCH_DB_USER_LIST"
export const RECEIVE_USER_LIST = "USER/RECEIVE_USER_LIST"

export function APIRequest() {
    return {
        type: API_REQUEST,
    }
}

export function APIResponse(result) {
    return {
        type: API_RESPONSE,
        result,
    }
}

export function setUser(user) {
    return {
        type: SET_USER,
        user,
    }
}

export function removeUser() {
    return {
        type: REMOVE_USER,
    }
}

export function updateDBUserDeviceID(uid, deviceId) {
    return {
        type: UPDATE_DB_USER_DEVICE_ID,
        uid,
        deviceId,
    }
}

export function fetchDBProfile(uid, resolve, reject) {
    return {
        type: FETCH_DB_PROFILE,
        uid,
        resolve,
        reject,
    }
}

export function updateDBProfile(profile, resolve, reject) {
    return {
        type: UPDATE_DB_PROFILE,
        profile,
        resolve,
        reject,
    }
}

export function receiveProfile(profile) {
    return {
        type: RECEIVE_PROFILE,
        profile,
    }
}

export function fetchDBUserList(resolve, reject) {
    return {
        type: FETCH_DB_USER_LIST,
        resolve,
        reject,
    }
}

export function receiveUserList(userList) {
    return {
        type: RECEIVE_USER_LIST,
        userList,
    }
}