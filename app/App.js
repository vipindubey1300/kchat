import React from 'react';
import { StatusBar, StyleSheet, Easing, Animated } from 'react-native';
import SplashScreen from 'react-native-splash-screen';

import { Provider } from 'react-redux';
import { store } from './Store';

import { createSwitchNavigator, createAppContainer } from 'react-navigation';

import AuthCheckStackNavigation from './Navigations/AuthCheckStackNavigation';
import SignUpStackNavigation from './Navigations/SignUpStackNavigation';
import MainStackNavigation from './Navigations/MainStackNavigation';

const AppStackNavigator = createSwitchNavigator({
		AuthCheckStackNavigation: AuthCheckStackNavigation,
		SignUpStackNavigation: SignUpStackNavigation,
		MainStackNavigation: MainStackNavigation,
	},
	{
		initialRouteName: 'AuthCheckStackNavigation',
		headerMode: 'none',
		mode: 'modal',

		defaultNavigationOptions: {
			gesturesEnabled: false,
		},

		transitionConfig: () => ({
			transitionSpec: {
				duration: 700,
				easing: Easing.out(Easing.poly(4)),
				timing: Animated.timing,
			},

			screenInterpolator: sceneProps => {
				const {position, scene} = sceneProps
				const {index} = scene

				const translateX = 0
				const translateY = 0

				const opacity = position.interpolate({
					inputRange: [index - 0.7, index, index + 0.7],
					outputRange: [0.0, 1, 1.0]
				})

				return {
					opacity,
					transform: [{translateX}, {translateY}]
				}
			},
		}),
	}
);

const RootNavigator = createAppContainer(AppStackNavigator);

export default class App extends React.Component {

	constructor(props) {
		super(props);
	}

	componentDidMount() {
		SplashScreen.hide();
	}

	render() {
		return (
			<Provider store={store}>
				<StatusBar hidden={true} />
				<RootNavigator />
			</Provider>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
});