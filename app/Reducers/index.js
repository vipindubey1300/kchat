import { combineReducers } from 'redux'

import { reducer as UserReducer } from "./UserReducer";
import { reducer as ChatReducer } from "./ChatReducer";

export default combineReducers({
	UserReducer,
	ChatReducer,
})