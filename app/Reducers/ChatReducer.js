import {
	API_REQUEST, 
	API_RESPONSE,
	RECEIVE_CHAT_PAIR_LIST,
	RECEIVE_CHAT_GROUP_LIST,
} from '../Actions/ChatActions'

const initialState = {
	chatPairList: null,
	chatGroupList: null,
	isFetching: false,
	isSuccess: false,
};

export function reducer(state = initialState, action) {
	switch(action.type) {
		case API_REQUEST:
			return { 
				...state, 
				isFetching: true,
				isSuccess: false, 
			}
		case API_RESPONSE:
			return { 
				...state, 
				isFetching: false,
				isSuccess: action.success, 
			}

		case RECEIVE_CHAT_PAIR_LIST:
			return { 
				...state, 
				chatPairList: action.chatPairList 
			}

		case RECEIVE_CHAT_GROUP_LIST:
			return { 
				...state, 
				chatGroupList: action.chatGroupList 
			}
		default:
			return state
	}
}