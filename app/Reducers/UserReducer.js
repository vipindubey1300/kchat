import { 
	API_REQUEST, 
	API_RESPONSE,
	SET_USER, 
	REMOVE_USER, 
	RECEIVE_PROFILE, 
	RECEIVE_USER_LIST	
} from '../Actions/UserActions'

const initialState = {
	userList: [],
	user: null,
	profile: null,
	isFetching: false,
	isSuccess: false,
};

export function reducer(state = initialState, action) {
	switch(action.type) {
		case API_REQUEST:
			return { 
				...state, 
				isFetching: true,
				isSuccess: false, 
			}
		case API_RESPONSE:
			return { 
				...state, 
				isFetching: false,
				isSuccess: action.success, 
			}
			
		case SET_USER:
			return { 
				...state, 
				user: action.user 
			}
		case REMOVE_USER:
			return Object.assign({}, initialState);
			

		case RECEIVE_PROFILE:
			return { 
				...state, 
				profile: action.profile 
			}

		case RECEIVE_USER_LIST:
			return { 
				...state, 
				userList: action.userList 
			}
	
		default:
			return state
	}
}