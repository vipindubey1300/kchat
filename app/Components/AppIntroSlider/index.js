import React from 'react';
import {
	StyleSheet,
	FlatList,
	View,
	Dimensions,
	Text,
	TouchableOpacity,
	Platform,
} from 'react-native';

import Colors from '../../Constants/Colors';

import PrimaryButton from '../../Components/Button/PrimaryButton'
import OutlineButton from '../../Components/Button/OutlineButton'

const { width, height } = Dimensions.get('window');

export default class AppIntroSlider extends React.Component {
	static defaultProps = {
		activeDotStyle: {
			backgroundColor: Colors.PRIMARY_COLOR,
		},
		dotStyle: {
			backgroundColor: 'rgba(255, 255, 255, 1)',
		},
	};
	state = {
		width,
		height,
		activeIndex: 0,
	};

	goToSlide = pageNum => {
		this.setState({ activeIndex: pageNum });
		this.flatList.scrollToOffset({
			offset: pageNum * this.state.width,
		});
	};

	// Get the list ref
	getListRef = () => this.flatList;

	_onNextPress = () => {
		this.goToSlide(this.state.activeIndex + 1);
		this.props.onSlideChange &&
			this.props.onSlideChange(this.state.activeIndex + 1, this.state.activeIndex);
	};

	_renderItem = item => {
		const { width, height } = this.state;
		const props = { ...item.item, width, height };
		return (
				this.props.renderItem(props)
		);
	};

	_renderButton = (name, onPress) => {
		return this.props[`render${name}Button`]
			? this.props[`render${name}Button`]()
			: name === 'Skip' ? this._renderOutlineButton(name, onPress) : this._renderDefaultButton(name, onPress);
	};

	_renderDefaultButton = (name, onPress) => {
		return (
			<PrimaryButton text={name} style={styles.primaryButton} onPress={onPress} />
	);
	};

	_renderOutlineButton = (name, onPress) => {
		return (
		<OutlineButton text={name} onPress={onPress} />
		);
	};

	_renderNextButton = () => this._renderButton('NEXT', this._onNextPress);

	_renderSignUpButton = () => this._renderButton('SIGN UP', this.props.onSignUp && this.props.onSignUp);

	_renderSkipButton = () =>
		this._renderButton('Skip', () =>
			this.props.onSkip ? this.props.onSkip() : this.goToSlide(this.props.slides.length - 1)
		);

	_renderPagination = () => {
		const isLastSlide = this.state.activeIndex === this.props.slides.length - 1;
		const isFirstSlide = this.state.activeIndex === 0;

		const skipBtn = !isLastSlide && this._renderSkipButton();
		const btn = isLastSlide ? this._renderSignUpButton() : this._renderNextButton();

		return (
			<View style={[styles.paginationContainer, this.props.paginationStyle]}>
				<View style={styles.paginationDots}>
					{this.props.slides.length > 1 &&
						this.props.slides.map((_, i) => (
							<TouchableOpacity
								key={i}
								style={{backgroundColor:'black', width: 14, height:14, margin:5, borderRadius:7}}
								onPress={() => this.goToSlide(i)}
							>
							<View
								key={i + this.props.slides.length}
								style={[
									styles.dot,
									i === this.state.activeIndex
										? this.props.activeDotStyle
										: this.props.dotStyle,
								]}
							/>
							</TouchableOpacity>
							
						))}
				</View>
				<View style={{flex:1, justifyContent:'flex-end', marginBottom: 40}}>
					<View style={{flexDirection:'row', justifyContent:'center'}}>
					{btn}
					</View>
					<View style={{flexDirection:'row', justifyContent:'center', marginTop:20,}}>
					{skipBtn}
					</View>
				</View>
			</View>
		);
	};

	_onMomentumScrollEnd = e => {
		const offset = e.nativeEvent.contentOffset.x;
		// Touching very very quickly and continuous brings about
		// a variation close to - but not quite - the width.
		// That's why we round the number.
		// Also, Android phones and their weird numbers
		const newIndex = Math.round(offset / this.state.width);
		if (newIndex === this.state.activeIndex) {
			// No page change, don't do anything
			return;
		}
		const lastIndex = this.state.activeIndex;
		this.setState({ activeIndex: newIndex });
		this.props.onSlideChange && this.props.onSlideChange(newIndex, lastIndex);
	};

	_onLayout = () => {
		const { width, height } = Dimensions.get('window');
		if (width !== this.state.width || height !== this.state.height) {
			// Set new width to update rendering of pages
			this.setState({ width, height });
			// Set new scroll position
			const func = () => {
				this.flatList.scrollToOffset({
					offset: this.state.activeIndex * width,
					animated: false,
				});
			};
			Platform.OS === 'android' ? setTimeout(func, 0) : func();
		}
	};

	render() {
		// Separate props used by the component to props passed to FlatList    
		return (
			<View style={styles.flexOne}>
				<FlatList
					ref={ref => (this.flatList = ref)}
					data={this.props.slides}
					horizontal
					pagingEnabled
					showsHorizontalScrollIndicator={false}
					bounces={false}
					style={styles.flatList}
					renderItem={this._renderItem}
					onMomentumScrollEnd={this._onMomentumScrollEnd}
					extraData={this.state.width}
					onLayout={this._onLayout}
				/>
				{this._renderPagination()}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	flexOne: {
		flex: 1,
		flexDirection: 'column',
	},
	flatList: {
		flex: 1,
		flexDirection: 'row',
	},
	paginationContainer: {
		flex: 0.5,
	},
	paginationDots: {
		height: 16,
		margin: 16,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
	},
	dot: {
		width: 10,
		height: 10,
		borderRadius: 5,
		margin: 2,
		backgroundColor:'white'
	},
	primaryButton: {
		flex: 0.8
	}
});
