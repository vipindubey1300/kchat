import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import FastImage from 'react-native-fast-image';
import FeatherIcon from 'react-native-vector-icons/Feather';
import Colors from '../../Constants/Colors';
import { withNavigation } from 'react-navigation';


class NavigationHeader extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.container}>
                {this.props.haveBack ? 
                <TouchableOpacity style={{width:100, alignItems:'flex-start'}} onPress={() => this.props.navigation.goBack()}>
                    <FeatherIcon name="arrow-left"  size={25} color={Colors.BLACK_COLOR} />
                </TouchableOpacity>
                :
                this.props.leftButtonLabel !== '' ?
                <TouchableOpacity style={{width:100, alignItems:'flex-start'}} onPress={this.props.leftButtonAction}>
                    <Text numberOfLines={1} style={styles.leftButtonLabelText}>{this.props.leftButtonLabel}</Text>
                </TouchableOpacity>
                :
                <View style={{width:100}} />
                }
                <View style={styles.markContainer}>
                    <FastImage
                        style={{ width: 60, height: 60 }}
                        source={require('../../Assets/Image/logo.png')}
                        resizeMode={FastImage.resizeMode.contain} />
                </View>
                {this.props.rightButtonLabel !== '' ?
                <TouchableOpacity style={{width:100, alignItems:'flex-end'}} onPress={this.props.rightButtonAction}>
                    <Text numberOfLines={1} style={styles.rightButtonLabelText}>{this.props.rightButtonLabel}</Text>
                </TouchableOpacity>
                :
                <View style={{width:100}} />
                }
            </View>
        );
    }
}

NavigationHeader.propTypes = {
    title: PropTypes.string.isRequired,
    onPress: PropTypes.func,
    haveBack: PropTypes.bool,
    showMark: PropTypes.bool,
    rightButtonLabel: PropTypes.string,
    rightButtonAction: PropTypes.func,
};
  
NavigationHeader.defaultProps = {
    title: 'KChat',
    haveBack: false,
    showMark: false,
    rightButtonLabel: "",
    rightButtonAction: () => console.log("Right Button Clicked"),
};

export default withNavigation(NavigationHeader);

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row', 
        height: 60, 
        alignItems: 'center', 
        justifyContent:'space-between',
        backgroundColor: Colors.PRIMARY_COLOR,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.3,
        shadowRadius: 2,
        elevation: 8,
        paddingHorizontal: 15,
    },

    title: {
        textAlign: 'center',
        color: Colors.BLACK_COLOR,
        fontSize: 20, 
    },

    markContainer: {
        flexDirection:'row', 
        alignItems:'center', 
        justifyContent: 'center',
    },

    largeMarkText: {
        textAlign: 'center',
        color: Colors.BLACK_COLOR,
        fontSize: 50, 
    },

    smallMarkText: {
        textAlign: 'center',
        color: Colors.BLACK_COLOR,
        fontSize: 15, 
        marginLeft: -10
    },

    rightButtonLabelText: {
        textAlign: 'center',
        color: Colors.BLACK_COLOR,
        fontSize: 18, 
    },

    leftButtonLabelText: {
        textAlign: 'center',
        color: Colors.BLACK_COLOR,
        fontSize: 18, 
    },
});