import React from 'react';
import { StyleSheet, View, TextInput} from 'react-native';
import Colors from '../../Constants/Colors';

export default class RoundTextInput extends React.Component {
	state = {
		text: this.props.defaultValue ? this.props.defaultValue : ''
	};

	componentWillReceiveProps(nextProps) {
		// You don't have to do this check first, but it can help prevent an unneeded render
		if (nextProps.defaultValue !== this.props.defaultValue) {
			this.setState({ text: nextProps.defaultValue.toString() });
		}
	}

	getInputValue = () => this.state.text;

	render() {
			return (
				<View style={[styles.container, this.props.style, this.props.error ? styles.containerError : {}]}>
					<TextInput
						style={styles.inputText}
						value={this.state.text}
						autoCapitalize={this.props.autoCapitalize}
						ref={ref => this.input = ref}
						autoCorrect={false}
						underlineColorAndroid='transparent'
						secureTextEntry={this.props.secureTextEntry}
						blurOnSubmit={this.props.blurOnSubmit}
						keyboardType={this.props.keyboardType}
						multiline={this.props.multiline ? this.props.multiline : false}
						returnKeyType={this.props.returnKeyType}
						placeholder={this.props.placeholder}
						editable={this.props.editable}
						textContentType={this.props.textContentType}
						onSubmitEditing={this.props.focus(this.props.placeholder)}
						placeholderTextColor={Colors.PLACEHOLDER_COLOR}
						onChangeText={this.props.onChangeText ? this.props.onChangeText : (text) => this.setState({ text })}
					/>
				</View>
			);
		}
	}

	RoundTextInput.defaultProps = {
		focus: () => {},
		style: {},
		placeholder: '',
		blurOnSubmit: false,
		returnKeyType: 'next',
		error: false,
		editable: true,
		keyboardType: null,
		secureTextEntry: false,
		autoCapitalize: "none",
		textContentType: "none",
		defaultValue: '',
		multiline: false,
	};

	const styles = StyleSheet.create({
		container: {
			backgroundColor: Colors.ROUND_TEXTINPUT_BACKGROUND_COLOR,
			flexDirection: 'row',
			alignItems: 'center',
			paddingVertical: 6,
			borderRadius: 10,
			borderColor: Colors.ROUND_TEXTINPUT_BORDER_COLOR,
			borderWidth: 1,
		},
		
		containerError: {
			backgroundColor: '#FFE0E0',
			borderWidth: 1,
			borderColor: '#E57373',
		},

		inputText: {
			color: 'black',
			flex: 1,
			paddingVertical: 0,
			marginVertical: 3,
			fontSize: 16,
			marginHorizontal: 17,
		},
});
