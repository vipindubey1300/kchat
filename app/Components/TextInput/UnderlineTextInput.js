import React from 'react';
import { StyleSheet, View, TextInput, Text} from 'react-native';
import Colors from '../../Constants/Colors';

export default class UnderlineTextInput extends React.Component {
  state = {
    text: this.props.defaultValue ? this.props.defaultValue : '',
    isFocus: false,
  };

	componentWillReceiveProps(nextProps) {
		// You don't have to do this check first, but it can help prevent an unneeded render
		if (nextProps.defaultValue !== this.props.defaultValue) {
			this.setState({ text: nextProps.defaultValue.toString() });
		}
	}

  getInputValue = () => this.state.text;

  render() {
    const { isFocus, text } = this.state;
    const showPlaceHolderTop = isFocus || text !== '';
    return (
      <View style={[styles.container, this.props.style]}>
        <Text style={{color: Colors.GREY, fontSize: 18, marginLeft: 3,}}>{showPlaceHolderTop ? this.props.placeholder : ''}</Text>
        <TextInput
          style={styles.inputText}
          value={this.state.text}
          autoCapitalize={this.props.autoCapitalize}
          ref={ref => this.input = ref}
          autoCorrect={false}
          onFocus={() => this.setState({isFocus:true})}
          onBlur={() => this.setState({isFocus:false})}
          underlineColorAndroid='transparent'
          secureTextEntry={this.props.secureTextEntry}
          blurOnSubmit={this.props.blurOnSubmit}
          keyboardType={this.props.keyboardType}
          returnKeyType={this.props.returnKeyType}
          placeholder={showPlaceHolderTop ? '' : this.props.placeholder}
          textContentType={this.props.textContentType}
          onSubmitEditing={this.props.focus(this.props.placeholder)}
          placeholderTextColor={Colors.GREY}onChangeText={this.props.onChangeText ? this.props.onChangeText : (text) => this.setState({ text })}
          editable={this.props.editable}
        />
      </View>
    );
  }
}

UnderlineTextInput.defaultProps = {
  focus: () => {},
  style: {},
  placeholder: '',
  blurOnSubmit: false,
  returnKeyType: 'next',
  keyboardType: null,
  secureTextEntry: false,
  autoCapitalize: "none",
  textContentType: "none",
  defaultValue: '',
  editable: true,
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 6,
    borderBottomColor: Colors.BLACK_COLOR,
    borderBottomWidth: 1,
  },

  inputText: {
    color: 'black',
    paddingVertical: 0,
    marginVertical: 3,
    fontSize: 18,
  },
});
