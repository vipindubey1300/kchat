import React from 'react';
import { StyleSheet, View, TextInput, Text} from 'react-native';
import Colors from '../../Constants/Colors';

export default class PhoneNumberInput extends React.Component {
  state = {
    text: this.props.defaultValue ? this.props.defaultValue : '',
    areaCode: this.props.areaCode ? this.props.areaCode : ''
  };

  getInputValue = () => this.state.text;

  render() {
    return (
      <View style={[styles.container, this.props.style, this.props.error ? styles.containerError : {}]}>
        <View style={{
          flexDirection: 'row',
          borderColor: Colors.ROUND_TEXTINPUT_BORDER_COLOR,
          borderBottomWidth: 1,
          alignItems: 'center',
          justifyContent: 'center',
          width: 200
        }}>
          <Text style={styles.areaCodeText}>{this.state.areaCode}</Text>
          <TextInput
            style={styles.inputText}
            value={this.state.text}
            autoCapitalize={this.props.autoCapitalize}
            ref={ref => this.input = ref}
            autoCorrect={false}
            underlineColorAndroid='transparent'
            secureTextEntry={this.props.secureTextEntry}
            blurOnSubmit={this.props.blurOnSubmit}
            keyboardType={this.props.keyboardType}
            returnKeyType={this.props.returnKeyType}
            textContentType={this.props.textContentType}
            onSubmitEditing={this.props.focus(this.props.placeholder)}
            placeholderTextColor={Colors.PRIMARY_COLOR}
            onChangeText={(text) => this.setState({ text })}
          />
        </View>
        <Text style={{fontSize: 12,}}>{this.props.placeholder}</Text>
      </View>
    );
  }
}

PhoneNumberInput.defaultProps = {
  focus: () => {},
  style: {},
  placeholder: '',
  areaCode: '',
  blurOnSubmit: false,
  returnKeyType: 'next',
  error: false,
  keyboardType: null,
  secureTextEntry: false,
  autoCapitalize: "none",
  textContentType: "none",
  defaultValue: '',
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "transparent",
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 6,
  },
  
  containerError: {
    backgroundColor: '#FFE0E0',
    borderWidth: 1,
    borderColor: '#E57373',
  },

  areaCodeText: {
    color: Colors.BLACK_COLOR,
    paddingVertical: 0,
    fontSize: 16,
    marginHorizontal: 10,
  },

  inputText: {
    color: Colors.BLACK_COLOR,
    paddingVertical: 0,
    fontSize: 16,
    minWidth: 60,
  },
});
