import React from 'react';
import PropTypes from 'prop-types';
import ModalSelector from 'react-native-modal-selector'
import { StyleSheet, View, Text, TextInput} from 'react-native';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import Colors from '../../Constants/Colors';

const ModalPicker = ({data, containerStyle, textStyle, value, placeHolder, onChange}) => {
    return (
        <ModalSelector
            data={data}
            style={[styles.container, containerStyle]} 
            initValue={placeHolder}
            onChange={onChange} 
        >
            <View style={{flexDirection:'row', alignItems:'center'}}>
                <TextInput
                    style={[styles.text, textStyle]}
                    editable={false}
                    placeholderTextColor="grey"
                    placeholder={placeHolder}
                    value = {value}
                />
                <FontAwesome5Icon name="chevron-down" size={20} color="grey" style={{marginRight:15}}/>
            </View>
        </ModalSelector>
    );
}

export default ModalPicker;

ModalPicker.propTypes = {
    containerStyle: PropTypes.object,
    textStyle: PropTypes.object,
    value : PropTypes.string,
    placeHolder: PropTypes.string.isRequired,
	onChange: PropTypes.func.isRequired,
};

ModalPicker.defaultProps = {
    containerStyle: {},
    textStyle: {},
    value: '',
    placeHolder: 'Please select a value',
    onChange: () => {},
};

const styles = StyleSheet.create({
    container: {
        flex:1,
        borderWidth:1, 
        paddingVertical: 6,
        borderRadius: 10,
        borderColor: Colors.ROUND_TEXTINPUT_BORDER_COLOR,
        backgroundColor: Colors.ROUND_TEXTINPUT_BACKGROUND_COLOR,
    },
  
    text: {
        color: 'black',
        flex: 1,
        paddingVertical: 0,
        marginVertical: 3,
        fontSize: 16,
        marginLeft: 17,
    },
});
