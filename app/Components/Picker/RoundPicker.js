import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, StyleSheet, View, Text} from 'react-native';
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import Colors from '../../Constants/Colors';

const RoundPicker = ({containerStyle, textStyle, text, onPress, icon, placeHolder}) => {
    return (
        <TouchableOpacity style={[styles.container, containerStyle]} onPress={onPress}>
            <View style={{flexDirection:'row', alignItems:'center'}}>
              	{text !== '' ? 
                  <Text style={[styles.text, textStyle]}>{text}</Text>
                  :
                  <Text style={[styles.placeHolder]}>{placeHolder}</Text>
                }
          		<FontAwesome5Icon name={icon} size={20} color="grey" style={{marginRight:15}}/>
            </View>
        </TouchableOpacity>
    );
}

export default RoundPicker;

RoundPicker.propTypes = {
    containerStyle: PropTypes.object,
    textStyle: PropTypes.object,
    placeHolder: PropTypes.string,
    text: PropTypes.string,
    onPress: PropTypes.func.isRequired,
    icon: PropTypes.string,
};

RoundPicker.defaultProps = {
  containerStyle: {},
  textStyle: {},
  placeHolder: 'Time',
  text: '',
  icon: "chevron-down",
  onPress: () => {},
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.ROUND_TEXTINPUT_BACKGROUND_COLOR,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 6,
    borderRadius: 10,
    borderColor: Colors.ROUND_TEXTINPUT_BORDER_COLOR,
    borderWidth: 1,
  },
  
  text: {
    color: 'black',
    flex: 1,
    paddingVertical: 0,
    marginVertical: 6,
    fontSize: 16,
    marginHorizontal: 20,
  },
  
  placeHolder: {
    color: 'grey',
    flex: 1,
    paddingVertical: 0,
    marginVertical: 3,
    fontSize: 16,
    marginLeft: 20,
  },
});
