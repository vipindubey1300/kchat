import React from 'react';
import { Text, StyleSheet, TouchableOpacity } from "react-native";
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import Colors from '../../Constants/Colors';

const RoundShadowButton = ({buttonStyle, textStyle, onPress, icon = '', text}) => {
    return (
        <TouchableOpacity style={[styles.button, buttonStyle]} onPress={onPress}>
            {icon !== '' && <FontAwesome5Icon name={icon}  size={18} color="black" />}
            <Text style={[styles.text, textStyle]}>{text}</Text>
        </TouchableOpacity>
    );
}

export default RoundShadowButton;

const styles = StyleSheet.create({
    button: {
        flexDirection:'row',
        backgroundColor: "white",
        borderRadius: 10,
        alignItems:'center',
        justifyContent: 'center',
        borderWidth: 2,
        borderColor: Colors.LIGHT_GREY,
        shadowColor: '#000',
        shadowRadius: 1,
        elevation: 3,
        shadowOpacity: 0.3,     
        shadowOffset: {
            width: 0,
            height: 2,
        },
    },
    text: {
        textAlign: 'center',
        color: 'black',
        marginVertical: 10,
        marginHorizontal: 10,
        fontSize: 14,
        fontWeight: "500",
    },
});