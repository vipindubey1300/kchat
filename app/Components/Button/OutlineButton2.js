import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import Colors from '../../Constants/Colors';

const OutlineButton2 = ({style, onPress, text1, text2}) => {
    return (
        <TouchableOpacity style={[styles.button, style]} onPress={onPress}>
            <View style={{flexDirection:'row'}}>
                <Text style={styles.text1}>{text1}</Text>
                <Text style={styles.text2}>{text2}</Text>
            </View>
        </TouchableOpacity>
    );
}

export default OutlineButton2;

const styles = StyleSheet.create({
    button: {
        backgroundColor: "transparent",
        borderRadius: 25,
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: 'black',
    },
    text1: {
        textAlign: 'center',
        color: 'black',
        marginVertical: 10,
        marginLeft: 20,
        fontSize: 16,
    },
    text2: {
        textAlign: 'center',
        color: Colors.PRIMARY_COLOR,
        marginVertical: 10,
        marginRight: 20,
        fontSize: 16,
        fontWeight: "500",
    },
});