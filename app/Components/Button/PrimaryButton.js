import React from 'react';
import { Text, StyleSheet, TouchableOpacity } from "react-native";
import Colors from '../../Constants/Colors';

const PrimaryButton = ({style, onPress, text}) => {
    return (
        <TouchableOpacity style={[styles.button, style]} onPress={onPress}
        >
            <Text style={styles.text}>{text}</Text>
        </TouchableOpacity>
    );
}

export default PrimaryButton;

const styles = StyleSheet.create({
    button: {
        backgroundColor: Colors.PRIMARY_COLOR,
        borderRadius: 25,
        justifyContent: 'center',
    },
    text: {
        textAlign: 'center',
        color: 'black',
        marginVertical: 10,
        marginHorizontal: 30,
        fontSize: 16,
        fontWeight: "600",
    },
});