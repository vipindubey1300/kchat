import React from 'react';
import PropTypes from 'prop-types';
import { Image, StyleSheet, TouchableOpacity } from "react-native";

const CircleImageButton = ({style, onPress, source}) => {
    return (
        <TouchableOpacity onPress={onPress}>
            <Image source={source} style={[styles.image, style]} ></Image>
        </TouchableOpacity>
    );
}

export default CircleImageButton;

CircleImageButton.propTypes = {
    source: PropTypes.number.isRequired,
    onPress: PropTypes.func.isRequired,
    style: PropTypes.object,
};
  
CircleImageButton.defaultProps = {
    source: null,
    onPress: () => {},
    style: null,
};

const styles = StyleSheet.create({
    image: {
        width: 70, 
        height: 70
    },
});