import React from 'react';
import { Text, StyleSheet, TouchableOpacity } from "react-native";
import Colors from '../../Constants/Colors';

const OutlineButton = ({style, onPress, text}) => {
    return (
        <TouchableOpacity style={[styles.button, style]} onPress={onPress}>
            <Text style={styles.text}>{text}</Text>
        </TouchableOpacity>
    );
}

export default OutlineButton;

const styles = StyleSheet.create({
    button: {
        backgroundColor: "transparent",
        borderRadius: 25,
        justifyContent: 'center',
        borderWidth: 1,
        borderColor: 'black',
    },
    text: {
        textAlign: 'center',
        color: Colors.PRIMARY_COLOR,
        marginVertical: 10,
        marginHorizontal: 20,
        fontSize: 16,
        fontWeight: "500",
    },
});