import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import FontAwesome5Icon from 'react-native-vector-icons/FontAwesome5';
import Colors from '../../Constants/Colors';

const MenuButton = ({containerStyle, textStyle, icon, onPress, text}) => {
    return (
        <TouchableOpacity style={[styles.container, containerStyle]} onPress={onPress}>
            <View style={{flexDirection:'row'}}>
                <Text style={[styles.text, textStyle]}>{text}</Text>
                {icon !== '' ? 
          		    <FontAwesome5Icon name={icon} size={15} color="black" style={{marginRight:15}}/>
                :
                    null
                }
            </View>
        </TouchableOpacity>
    );
}

export default MenuButton;

MenuButton.propTypes = {
    containerStyle: PropTypes.any,
    textStyle: PropTypes.object,
    text: PropTypes.string,
    onPress: PropTypes.func.isRequired,
    icon: PropTypes.string,
};

MenuButton.defaultProps = {
  containerStyle: {},
  textStyle: {},
  text: '',
  icon: '',
  onPress: () => {},
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.PRIMARY_COLOR,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 10,
    borderWidth: 0,
  },
  
  text: {
    color: 'black',
    flex: 1,
    paddingTop: 0,
    paddingBottom: 0,
    fontSize: 16,
    marginHorizontal: 15,
  },  
});
