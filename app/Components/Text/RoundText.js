import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text} from 'react-native';
import Colors from '../../Constants/Colors';

const RoundText = ({containerStyle, textStyle, text}) => {
    return (
        <View style={[styles.container, containerStyle]}>
            <Text style={[styles.text, textStyle]}>{text}</Text>
        </View>
    );
}

export default RoundText;

RoundText.propTypes = {
    containerStyle: PropTypes.object,
    textStyle: PropTypes.object,
    text: PropTypes.string.isRequired,
};

RoundText.defaultProps = {
  containerStyle: {},
  textStyle: {},
  text: '',
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.ROUND_TEXTINPUT_BACKGROUND_COLOR,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 10,
    borderColor: Colors.ROUND_TEXTINPUT_BORDER_COLOR,
    borderWidth: 1,
  },
  
  text: {
    color: 'black',
    flex: 1,
    paddingTop: 0,
    paddingBottom: 0,
    fontSize: 16,
    marginLeft: 20,
  },
});
