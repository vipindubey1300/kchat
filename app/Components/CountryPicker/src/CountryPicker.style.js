import { StyleSheet, PixelRatio } from 'react-native'
import { getHeightPercent } from './ratio'
import {
	Dimensions,
} from 'react-native'
import Colors from '../../../Constants/Colors';
const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
	container: {
		marginTop: 20,
	},

	modalContainer: {
		backgroundColor: 'white',
		flex: 1
	},

	contentContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		backgroundColor: 'white'
	},

	header: {
		flexDirection: 'row',
		alignItems: 'center'
	},

	input: {
		height: 48,
		width: '70%'
	},

	inputOnly: {
		marginLeft: '15%'
	},

	touchFlag: {
		alignItems: 'center',
		justifyContent: 'center',
	},

	imgStyle: {
		resizeMode: 'contain',
		width: 35,
		height: 25,
		borderWidth: 1,
		borderColor: '#eee',
	},

	emojiFlag: {
		alignItems: 'center',
		justifyContent: 'center',
		fontSize: 30,
		width: 30,
		height: 30,
		borderWidth: 1 / PixelRatio.get(),
		borderColor: 'transparent',
		backgroundColor: 'transparent'
	},

	itemCountry: {
		flexDirection: 'row',
		height: 45,
		justifyContent: 'flex-start',
		alignItems: 'center',
		borderBottomWidth: 1,
		borderBottomColor: '#ccc',
	},

	itemCountryFlag: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 45,
		marginHorizontal: 10,
	},

	itemCountryName: {
		justifyContent: 'center',
		width: width - 20,
		height: 20
	},

	countryName: {
		fontSize: 14
	},

	scrollView: {
		flex: 1
	},

	letters: {
		backgroundColor: '#eee',
		justifyContent: 'center',
		alignItems: 'center'
	},

	letter: {
		height: 25,
		width: 40,
		justifyContent: 'center',
		alignItems: 'center'
	},

	letterText: {
		textAlign: 'center',
		fontSize: getHeightPercent(2.2)
	},

	closeButton: {
		height: 48,
		width: '15%',
		alignItems: 'center',
		justifyContent: 'center'
	},
	
	closeButtonImage: {
		height: 24,
		width: 24,
		resizeMode: 'contain'
	}
})
