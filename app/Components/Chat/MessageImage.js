import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Image, StyleSheet, View, ViewPropTypes, ActivityIndicator} from 'react-native';
// @ts-ignore
import Lightbox from '../CustomLightBox/Lightbox';

const styles = StyleSheet.create({
    container: {
        width: 156,
        height: 110,
    },
    image: {
        width: 150,
        height: 100,
        borderRadius: 13,
        margin: 3,
        resizeMode: 'cover',
    },
    imageActive: {
        flex: 1,
        resizeMode: 'contain',
    },
    preloader: {
        right: 0,
        bottom: 0,
        top: 0,
        left: 0,
        position: 'absolute',
        alignSelf: 'center',
        justifyContent: 'center',
    },
});
export default class MessageImage extends Component {
    render() {
        const { containerStyle, lightboxProps, imageProps, imageStyle, currentMessage, } = this.props;
        if (!!currentMessage) {
            return (
                <View style={[styles.container, containerStyle]}>
                    <Lightbox activeProps={{ style: styles.imageActive, }} onLongPress = {() => lightboxProps.onLongPress(currentMessage)} >
                        <Image {...imageProps} style={[styles.image, imageStyle]} source={{ uri: currentMessage.image }}/>
                    </Lightbox>
                </View>
            );
        }
        return null;
        // else {
        //     return <View style={[styles.container, containerStyle]}>
        //         <View style={styles.preloader}>
        //             <ActivityIndicator />
        //         </View>
        //     </View>
        // }
    }
}
MessageImage.defaultProps = {
    currentMessage: {
        image: null,
    },
    containerStyle: {},
    imageStyle: {},
    imageProps: {},
    lightboxProps: {},
};
MessageImage.propTypes = {
    currentMessage: PropTypes.object,
    containerStyle: ViewPropTypes.style,
    imageStyle: PropTypes.object,
    imageProps: PropTypes.object,
    lightboxProps: PropTypes.object,
};