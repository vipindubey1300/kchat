import React from 'react'
import { 
    TextInput, 
    Keyboard, 
    Dimensions, 
    Animated, 
    UIManager,
    TouchableWithoutFeedback 
} from 'react-native'
import { getStatusBarHeight } from 'react-native-iphone-x-helper'

const { State: TextInputState } = TextInput;
const statusBarHeight = getStatusBarHeight();
// Component
export default class KeyboardVisible extends React.Component {	

    constructor(props) {
        super(props)

        this.state = {
            shift: new Animated.Value(0),
        }
    }

    componentWillMount() {
        this.keyboardDidShowSub = Keyboard.addListener('keyboardDidShow', this.handleKeyboardDidShow);
        this.keyboardDidHideSub = Keyboard.addListener('keyboardDidHide', this.handleKeyboardDidHide);
    }

    componentWillUnmount() {
        this.keyboardDidShowSub.remove();
        this.keyboardDidHideSub.remove();
    }

    handleKeyboardDidShow = (event) => {
        const { height: windowHeight } = Dimensions.get('window');
        const keyboardHeight = event.endCoordinates.height;
        const currentlyFocusedField = TextInputState.currentlyFocusedField();
        if(currentlyFocusedField === null) return;
        UIManager.measure(currentlyFocusedField, (originX, originY, width, height, pageX, pageY) => {
            const fieldHeight = height;
            const fieldTop = pageY;
            const gap = (windowHeight - keyboardHeight) - (fieldTop + fieldHeight);
            if (gap - 40 >= 0) {
                return;
            }
            Animated.timing(
                this.state.shift,
                {
                toValue: gap - 40,
                duration: 500,
                useNativeDriver: true,
                }
            ).start();
        });
    }

    handleKeyboardDidHide = () => {
        Animated.timing(
        this.state.shift,
        {
            toValue: 0,
            duration: 500,
            useNativeDriver: true,
        }
        ).start();
    }

  render() {
    const { shift } = this.state;
    return (
        <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
            <Animated.View style={{justifyContent: 'flex-start', flex: 1, transform: [{translateY: shift}] }}>
                {this.props.children}
            </Animated.View>
        </TouchableWithoutFeedback>
    )
  }
}