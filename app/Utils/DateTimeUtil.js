import moment from 'moment-timezone';

export const DATE_TO_12_HOUR_FORMAT_TIME_STRING = (date) => {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+ minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
};

export const DATE_TO_UTC_24_HOUR_FORMAT_TIME_STRING = (date) => {
    var m = moment.utc(date);
    var hours = m.hours();
    var minutes = m.minutes();
    var strTime = hours + ':' + minutes;
    return strTime;
};

export const UTC_TIME_TO_LOCAL_12_HOUR_FORMAT_TIME_STRING = (timeStr) => {
    return moment.utc(timeStr, 'HH:mm:ss').local().format('hh:mm A');
};

export const TIMESTAMP_TO_DATE_STRING = (ts, format="YYYY-MM-DD") => {
    return moment(ts).format(format);
};

export const ENUMERATE_DAYS_BETWEEN_DATES = (startDate, endDate, format="YYYY-MM-DD") => {
    var dates = [];

    var currDate = moment(startDate).startOf('day');
    var lastDate = moment(endDate).startOf('day');
    dates.push(currDate.clone().format(format));

    while(currDate.add(1, 'days').diff(lastDate) <= 0) {
        dates.push(currDate.clone().format(format));
    }

    return dates;
};

export const GET_CURRENT_DATE_STRING = (format="YYYY-MM-DD") => {
    return moment(new Date()).format(format);
}

export function timeAgoString(past) {
    const dayLengthInMs = 1000 * 60 * 60 * 24;
    const diff = new Date().getTime() - past;
    if(diff > dayLengthInMs)
        return moment(past).format("YYYY-MM-DD");
    else
        return moment(past).fromNow();
}