import firebase from 'react-native-firebase';
const axios = require('axios');
import Config from '../Constants/Config';

export const registerUserFCMToken = async (uid, token) => {
	return firebase.database().ref('users/' + uid).update({token:token})
	.then(function() {
		console.log('User token successfully updated.');
		return true;
	})
	.catch(function(error) {
		console.error("Error updating user token: ", error);
		return false;
	});
}

export const unregisterUserFCMToken = async (uid) => {
	return firebase.database().ref('users/' + uid + '/token').remove()
	.then(function() {
		console.log('User token successfully deleted.');
		return true;
	})
	.catch(function(error) {
		console.error("Error deleting user token: ", error);
		return false;
	});
}

export const sendNotificationToUser = async (uid, data) => {
	firebase.database().ref('users/' + uid).once('value')
	.then((snapshot) => {
        console.log('User profile successfully fetched.');
        const userInfo = snapshot.val();
        const token = userInfo && userInfo.token ? userInfo.token : null;
        // const isLoggedIn = userInfo && userInfo.is_logged_in ? userInfo.is_logged_in : false;
        // if(isLoggedIn && (token !== null && token !== undefined))
        if(token !== null && token !== undefined)
            sendNotificationToToken(token, data);
	})
	.catch((error) => {
		console.error("Error fetching user token: ", error);
		return null;
	});
}

export const sendNotificationToToken = async (token, data) => {
  data['to'] = token;
	axios({
    method: 'post',
    url: 'https://fcm.googleapis.com/fcm/send',
    headers: {
        "Content-Type": "application/json",
        "Authorization": `key=${Config.firebase.fcmKey}`
    },
    data: data
  });
}

export const getFCMToken = async () => {
    try {
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
          console.log("permission enabled")
          return await retrieveToken();
        } else {
          console.log("permission not enabled")
          return await requestPermission();
        }
    } catch (error) {
        // User has rejected permissions
        console.log('checkPermission error occured.' + error);
    }
    return null;
};

const retrieveToken = async () => {
    var fcmToken = null;
    try {
      fcmToken = await firebase.messaging().getToken();
      console.log("fcmToken = " + fcmToken)
    } catch (error) {
      // User has rejected permissions
      console.log('getToken error occured.' + error);
    }
    return fcmToken;
}


const requestPermission = async () => {
    try {
      console.log("requesting permission")
      await firebase.messaging().requestPermission();
      // User has authorised
      return await retrieveToken();
    } catch (error) {
      // User has rejected permissions
      console.log('permission rejected');
    }
}