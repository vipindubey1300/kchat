import firebase from 'react-native-firebase';

export const UPLOAD_AVATAR = (uri, uid, dir, mime = 'image/jpg') => {
    return new Promise((resolve, reject) => {
        const ref = firebase.storage().ref(`${dir}`).child(`${uid}`);

        ref.put(uri, { contentType: mime }).then( () => {
            ref.getDownloadURL().then((url) => {
                resolve(url);
            });
        })
        .catch((error) => {
            console.log(error);
            reject(null);
        });
    })
};

export const UPLOAD_CHAT_PHOTO = (uri, uid, dir, mime = 'image/jpg') => {
    return new Promise((resolve, reject) => {
        let rand = Math.random().toString(36).substring(2, 15);
        const ref = firebase.storage().ref(`${dir}`).child(`${uid}-${rand}`);

        ref.put(uri, { contentType: mime }).then( () => {
            ref.getDownloadURL().then((url) => {
                resolve(url);
            });
        })
        .catch((error) => {
            console.log(error);
            reject(null);
        });
    })
};
