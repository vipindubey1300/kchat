import { 
    Alert,
    Platform
} from 'react-native';
import RNFetchBlob from "rn-fetch-blob";

export const DOWNLOAD_PHOTO_FILE = (url) => {
    if(Platform.OS === 'ios') {
        IOS_DOWNLOAD_PHOTO_FILE(url);
    }
    else if(Platform.OS === 'android') {
        ANDROID_DOWNLOAD_PHOTO_FILE(url);
    }
};

const ANDROID_DOWNLOAD_PHOTO_FILE = (url) => {
    const DownloadDir = RNFetchBlob.fs.dirs.DownloadDir;
    RNFetchBlob
    .config({
        addAndroidDownloads : {
            useDownloadManager : true, // <-- this is the only thing required
            // Show notification when response data transmitted
            notification : true,
            // Optional, but recommended since android DownloadManager will fail when
            // the url does not contains a file extension, by default the mime type will be
            mime : 'image/jpeg',
            description : 'File downloaded by download manager.',
            // Make the file scannable  by media scanner
            mediaScannable : true,
            path:  DownloadDir + '/kchat-photo-download.jpg',
        }
    })
    .fetch('GET', url)
    .then((res) => {
        // the path of downloaded file
        const path = res.path();
    })
}


const IOS_DOWNLOAD_PHOTO_FILE = (url) => {
    const DownloadDir = RNFetchBlob.fs.dirs.DocumentDir;
    RNFetchBlob
    .config({
        addAndroidDownloads : {
            useDownloadManager : true, // <-- this is the only thing required
            // Show notification when response data transmitted
            notification : true,
            // Optional, but recommended since android DownloadManager will fail when
            // the url does not contains a file extension, by default the mime type will be
            mime : 'image/jpeg',
            description : 'File downloaded by download manager.',
            // Make the file scannable  by media scanner
            mediaScannable : true,
        },
        path:  DownloadDir + '/kchat-photo-download.jpg',
    })
    .fetch('GET', url)
    .then((res) => {
        // the path of downloaded file
        const path = res.path();
    })
}